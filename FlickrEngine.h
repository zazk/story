//
//  FlickrEngine.h
//  MKNetworkKit-iOS-Demo
//
//  Created by Mugunth Kumar (@mugunthkumar) on 22/1/12.
//  Copyright (C) 2011-2020 by Steinlogic

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
#import "MKNetworkKit/MKNetworkEngine.h"
//#warning Insert your flickr key here
#define FLICKR_KEY @"d9db022d9c3013ee39b2ee35c5b58774"
#define FLICKR_USER @"80833684%40N02"
#define FLICKR_URL @"services/rest/?method=flickr.photosets.%@&api_key=%@&user_id=%@&format=json&nojsoncallback=1"
#define FLICKR_SET_URL @"services/rest/?method=flickr.photosets.%@&api_key=%@&photoset_id=%@&format=json&nojsoncallback=1"
#define FLICKR_SETS @"getList"
#define FLICKR_PHOTOS @"getPhotos"
#define FLICKR_SETS_URL [NSString stringWithFormat:FLICKR_URL,FLICKR_SETS,FLICKR_KEY, FLICKR_USER]
#define FLICKR_PHOTOS_URL(__SET__) [NSString stringWithFormat:FLICKR_SET_URL, FLICKR_PHOTOS,FLICKR_KEY, __SET__]

@interface FlickrEngine : MKNetworkEngine

typedef void (^FlickrImagesResponseBlock)(NSMutableArray* imageURLs); 
-(void) imagesForKey:(NSString*) key subkey:(NSString*) subkey 
        onCompletion:(FlickrImagesResponseBlock) imageURLBlock
             onError:(MKNKErrorBlock) errorBlock;
-(void) imagesForKey:(NSString*) key subkey:(NSString*) subkey set:(NSString*) set
        onCompletion:(FlickrImagesResponseBlock) imageURLBlock
             onError:(MKNKErrorBlock) errorBlock;
@end
