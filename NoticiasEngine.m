//
//  NoticiasEngine.m
//  Story
//
//  Created by zazk on 15/09/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import "NoticiasEngine.h"

@implementation NoticiasEngine

-(void)newsWithIndex:(int ) index
           andPage:(int ) page
       onCompletion:(NoticiaResponseBlock) imageURLBlock
            onError:(MKNKErrorBlock) errorBlock {
    NSLog(@"Noticias URL SETS %@",NOTICIAS_URL(index, page));
    MKNetworkOperation *op = [self operationWithPath:NOTICIAS_URL(index, page)];
    
    [op onCompletion:^(MKNetworkOperation *completedOperation) { 
        NSDictionary *response = [completedOperation responseJSON]; 
        imageURLBlock([response objectForKey:@"noticia"]); 
    } onError:^(NSError *error) { 
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
}



-(NSString*) cacheDirectoryName {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *cacheDirectoryName = [documentsDirectory stringByAppendingPathComponent:@"NoticiasImages"];
    return cacheDirectoryName;
}
@end
