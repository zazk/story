//
//  EventosEngine.h
//  Story
//
//  Created by zazk on 14/09/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import "MKNetworkEngine.h" 
#define EVENTOS_URL [NSString stringWithFormat:@"postgrado/_vti_bin/UP.GIIT.Portal.EPG/EPGAppService.svc/evento/?pageIndex=0&itemsPerPage=20"]

@interface EventosEngine : MKNetworkEngine

typedef void (^EventoResponseBlock)(NSMutableArray* imageURLs);
-(void) onCompletion:(EventoResponseBlock) imageURLBlock
        onError:(MKNKErrorBlock) errorBlock;

@end
