//
//  MaestriaDetalleViewController.m
//  Story
//
//  Created by apple on 8/08/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import "MaestriaDetalleViewController.h"

@interface MaestriaDetalleViewController ()

@end

@implementation MaestriaDetalleViewController{
    NSString *sharer;
}
@synthesize webDetailText;
@synthesize bordeView;
@synthesize webVideo;

@synthesize maestriaLabel,maestriaName,detalle,colorHEX,
    urlvideo,url,link,brochure,sharePicker,sharePickerPopover;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidAppear:(BOOL)animated{
    
    [self setLandscapeImages];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    maestriaLabel.textColor = [UIColor colorWithRed:188.0/255.0 green:149.0/255.0 blue:88.0/255.0 alpha:1.0]; 
    maestriaLabel.text = maestriaName;
    
    [webDetailText setOpaque:NO];
    [webDetailText loadHTMLString:[NSString stringWithFormat:kBaseHTML, detalle] baseURL:nil];
    
    bordeView.backgroundColor = maestriaLabel.textColor = [UIColor colorWithHexString:colorHEX];
    
    //Add buttons  
    addButton(self,@"Ver el brochure",CGRectMake(40, 460, 150, 34),
              @selector(buttonBrochurePressed:) );
    addButton(self,@"Visite nuestra website",CGRectMake(200, 460, 210, 34),
              @selector(buttonURLPressed:) );
    addButton(self,@"Compartir",CGRectMake(420 ,460, 100, 34),
              @selector(setShareButtonTapped:) );
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        
        self.maestriaLabel.font = [UIFont boldSystemFontOfSize:15.0];
        [self.maestriaLabel setFrame:CGRectMake(20,
                                           10,
                                           300,
                                           40)];
        [self.webVideo setFrame:CGRectMake(20,
                                           60,
                                           self.maestriaLabel.bounds.size.width-10,
                                           180)];
        [self.webDetailText setFrame:CGRectMake(20,
                                           260,
                                           self.maestriaLabel.bounds.size.width-10,
                                                180)];
        [self.bordeView setFrame:CGRectMake(0,
                                                0,
                                                10,
                                                self.bordeView.bounds.size.height)];
        
        [self embedYouTube: getYoutubeID(urlvideo) ];
    }
    
    [self setLandscapeImages];
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
- (void)viewDidUnload
{ 
    [self setBordeView:nil]; 
    [self setWebDetailText:nil];
    [self setWebVideo:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

-(IBAction)done:(id)sender{
    [self dismissModalViewControllerAnimated:YES];
}

- (void)embedYouTube:(NSString *)urlString   {
    NSString *html = [NSString stringWithFormat:kEmbedHTML , urlString, webVideo.frame.size.width, webVideo.frame.size.height];
    [webVideo loadHTMLString:html baseURL:nil];  
}
 

-(void)buttonURLPressed:(id)sender { 
    link = url;
    [self performSegueWithIdentifier:@"ShowMaestriaURL" sender:self];
}

-(void)buttonBrochurePressed:(id)sender { 
    link = brochure;
    [self performSegueWithIdentifier:@"ShowMaestriaURL" sender:self];
}

-(void)setShareButtonTapped:(id)sender {
    if (sharePicker == nil) {
        self.sharePicker = [[SharePickerViewController alloc]
                        initWithStyle:UITableViewStylePlain];
        sharePickerPopover = [[UIPopoverController alloc] initWithContentViewController:sharePicker];
        
        [sharePicker setDelegate:self];
    }
    [sharePickerPopover presentPopoverFromRect:[sender bounds] inView:sender permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"ShowMaestriaURL"]) { 
        NavegadorViewController  *destViewController = segue.destinationViewController;
        destViewController.url = link;
    }
}

// Add to end of file
- (void)socialSelected:(NSString *)social {
    sharer = social;
    link =  [NSString stringWithFormat:social , encodeURL( url ), encodeURL( maestriaName )]; 
    [self performSegueWithIdentifier:@"ShowMaestriaURL" sender:self];
    [self.sharePickerPopover dismissPopoverAnimated:YES];
}

-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    
    [self setLandscapeImages];
     
}

-(void) setLandscapeImages{
    //bool isLandscape = UIDeviceOrientationIsLandscape([[UIDevice currentDevice] orientation]);
    
    self.view.backgroundColor =  [UIColor colorWithPatternImage:[UIImage imageNamed:isLandscape  ? @"bg-page-h.jpg": @"bg-page.png"]]; 
}

@end
