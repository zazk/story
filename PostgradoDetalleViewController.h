//
//  PostgradoDetalleViewController.h
//  Story
//
//  Created by apple on 23/08/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavegadorViewController.h"
#import "Common.h"

@interface PostgradoDetalleViewController : UIViewController <UIWebViewDelegate>
@property (strong, nonatomic) IBOutlet UILabel *lblTitulo;
@property (strong, nonatomic) IBOutlet UIWebView *webDetalle;
@property (strong, nonatomic) NSString *titulo;
@property (strong, nonatomic) NSString *html;
@property (strong, nonatomic) NSString *enlace;

@end
