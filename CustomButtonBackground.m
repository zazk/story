//
//  CustomButtonBackground.m
//  Story
//
//  Created by apple on 21/08/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import "CustomButtonBackground.h"
#import "Common.h"
#import "UIColor-Expanded.h"

@implementation CustomButtonBackground

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.

- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGColorRef whiteColor = [UIColor colorWithHexString:@"ffffff"].CGColor;
    CGColorRef lightGrayColor = [UIColor colorWithHexString:@"ff0000"].CGColor;
    
    CGRect paperRect = self.bounds;
    
    drawLinearGradient(context, paperRect, whiteColor, lightGrayColor);
}


@end
