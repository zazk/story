//`
//  CustomCellBackground.m
//  Cool
//
//  Created by apple on 28/07/12.
//
//

#import "CustomCellBackground.h"
#import "UIColor-Expanded.h"
#import "Common.h"

@implementation CustomCellBackground
 

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.

- (void)drawRect:(CGRect)rect
{ 
    drawLinearGradient(UIGraphicsGetCurrentContext(),
                       self.bounds,
                       [UIColor colorWithHexString:@"ffffff"].CGColor,
                       [UIColor colorWithHexString:@"cccccc"].CGColor);
}


@end
