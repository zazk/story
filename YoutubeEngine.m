//
//  YoutubeEngine.m
//  Story
//
//  Created by apple on 8/09/12.
//  Copyright (c) 2012 apple. All rights reserved.
//


#import "YoutubeEngine.h"

@implementation YoutubeEngine

-(void) onCompletion:(YoutubeImagesResponseBlock) imageURLBlock onError:(MKNKErrorBlock) errorBlock {
    NSLog(@"Youtube URL SETS %@",YOUTUBE_STRING);
    MKNetworkOperation *op = [self operationWithPath:YOUTUBE_STRING];
    
    [op onCompletion:^(MKNetworkOperation *completedOperation) {
        
        NSDictionary *response = [completedOperation responseJSON]; 
         
        imageURLBlock([[response objectForKey:@"feed"] objectForKey:@"entry"]);
        
    } onError:^(NSError *error) {
        
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
}



-(NSString*) cacheDirectoryName {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *cacheDirectoryName = [documentsDirectory stringByAppendingPathComponent:@"YoutubeImages"];
    return cacheDirectoryName;
}

@end
