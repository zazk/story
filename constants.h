//
//  Constants.h
//  Story
//
//  Created by apple on 3/09/12.
//  Copyright (c) 2012 apple. All rights reserved.
//
 
 
extern NSString * const kBaseHTML;
extern NSString * const kEmbedHTML;
extern NSString * const kEmbedHTMLiPhone;
extern NSString * const kEmbedYoutubeHTML;
extern NSString * const kHtmlWithYoutube;
extern NSString * const kHtmlWithYoutubeiPhone;
 