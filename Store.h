//
//  Store.h
//  Story
//
//  Created by zazk on 17/09/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Store : NSObject
{
    // Place any "global" variables here 
}
// message from which our instance is obtained
@property (strong, nonatomic) NSMutableDictionary *myData;
+ (Store *)sharedInstance;

@end
