//
//  Constants.m
//  Story
//
//  Created by apple on 3/09/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import "Constants.h"
 
NSString *const kBaseHTML =  @"<html><head>\
<style type=\"text/css\">\
ul,li,table,body,html,p,h1,h2,h3,h4,span,a,b,strong {font-size:13px !important;font-family: Helvetica !important;line-height:14px !important;}\
a {text-decoration:none;} img{max-width: 90%%; width: auto; height: auto !important; }\
body {\
margin:0px;padding:0px;overflow:hidden\
}\
</style>\
    </head><body style='margin:0 auto;margin:0;padding:20px 0;background-color: transparent;font:14px Helvetica !important;text-align:justify;'>%@</body></html>";


NSString *const kEmbedYoutubeHTML = @"\
<html><head>\
<style type=\"text/css\">\
ul,li,table,body,html,p {font-size:14px;}\
body {\
margin:0px;padding:0px;overflow:hidden\
}\
</style>\
</head><body style=\"margin:0\">\
<iframe  style='overflow:hidden;'\
    src='http://www.youtube.com/embed/%@' width='%0.0f' height='%0.0f' frameborder='0' allowfullscreen></iframe>\
</body></html>";

NSString *const kHtmlWithYoutube = @"\
    <html><head>\
    <style type=\"text/css\">\
    body {\
    margin:0px;padding:0px;overflow:hidden}\
    </style>\
    </head><body style=\"margin:0\"> %@\
    <div align='center' style='padding:20px 0'> <iframe width='100%%' height='400'  style='overflow:hidden;'\
    src='http://www.youtube.com/embed/%@' frameborder='0' allowfullscreen></iframe>\
    </div></body></html>";
 

NSString *const kHtmlWithYoutubeiPhone = @"\
    <html><head>\
    <style type=\"text/css\">\
    body {\
    margin:0px;padding:0px;overflow:hidden}\
    </style>\
    </head><body style=\"margin:0\"> %@\
    <div align='center' style='padding:20px 0'> <iframe width='100%%' height='200'  style='overflow:hidden;'\
    src='http://www.youtube.com/embed/%@' frameborder='0' allowfullscreen></iframe>\
    </div></body></html>";

NSString *const kEmbedHTML = @"\
<html><head>\
<style type=\"text/css\">\
body {\
background:#000;margin:0px;padding:0px;overflow:hidden\
}\
</style>\
</head><body style=\"margin:0\">\
<iframe style='overflow:hidden;'\
src='http://www.youtube.com/embed/%@' alt='%0.0f' alt='%0.0f' width='100%%' height='600'  frameborder='0' allowfullscreen></iframe>\
</body></html>";

NSString *const kEmbedHTMLiPhone = @"\
<html><head>\
<style type=\"text/css\">\
body {\
background:#000;margin:0px;padding:0px;overflow:hidden\
}\
</style>\
</head><body style=\"margin:0\">\
<iframe style='overflow:hidden;'\
src='http://www.youtube.com/embed/%@' alt='%0.0f' alt='%0.0f' width='100%%' height='200'  frameborder='0' allowfullscreen></iframe>\
</body></html>";