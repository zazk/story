//
//  EventosEngine.m
//  Story
//
//  Created by zazk on 14/09/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import "EventosEngine.h"

@implementation EventosEngine


-(void) onCompletion:(EventoResponseBlock) imageURLBlock
            onError:(MKNKErrorBlock) errorBlock {
    NSLog(@"Eventos URL SETS %@",EVENTOS_URL);
    MKNetworkOperation *op = [self operationWithPath:EVENTOS_URL];
    
    [op onCompletion:^(MKNetworkOperation *completedOperation) {
        
        NSDictionary *response = [completedOperation responseJSON];
        
        imageURLBlock([response objectForKey:@"evento"]);
        
    } onError:^(NSError *error) {
        
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
}



-(NSString*) cacheDirectoryName {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *cacheDirectoryName = [documentsDirectory stringByAppendingPathComponent:@"EventosImages"];
    return cacheDirectoryName;
}

@end
