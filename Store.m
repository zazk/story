//
//  Store.m
//  Story
//
//  Created by zazk on 17/09/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import "Store.h"

@implementation Store
@synthesize myData;

+ (Store *)sharedInstance
{
    // the instance of this class is stored here
    static Store *myInstance = nil;
    
    // check to see if an instance already exists
    if (nil == myInstance) {
        myInstance  = [[[self class] alloc] init];
        // initialize variables here
    }
    // return the instance of this class
    return myInstance;
}

@end
