//
//  DiplomadoDetalleViewController.m
//  Story
//
//  Created by apple on 8/08/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import "DiplomadoDetalleViewController.h"

@interface DiplomadoDetalleViewController ()

@end

@implementation DiplomadoDetalleViewController
@synthesize webDetalle;
@synthesize maestriaImage;
@synthesize maestriaLabel,maestriaName,detalle,imagen,url,
sharePicker,sharePickerPopover, brochure,link;
@synthesize loadingImageURLString,imageLoadingOperation;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidAppear:(BOOL)animated{
    
    [self setLandscapeImages];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [self setLandscapeImages];
    
    maestriaLabel.text = maestriaName;
    [webDetalle setOpaque:NO];
    [webDetalle loadHTMLString:[NSString stringWithFormat:kBaseHTML, detalle] baseURL:nil];
    

    //Add buttons
    
    
    addButton(self,@"Ver el brochure",CGRectMake(40, 500, 150, 34),
              @selector(buttonBrochurePressed:) );
    addButton(self,@"Visite nuestra website",CGRectMake(200, 500, 210, 34),
              @selector(buttonURLPressed:) );
    addButton(self,@"Compartir",CGRectMake(420 ,500, 100, 34),
              @selector(setShareButtonTapped:) );
    
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        
        self.maestriaLabel.font = [UIFont boldSystemFontOfSize:15.0];
        [self.maestriaLabel setFrame:CGRectMake(20,
                                                10,
                                                300,
                                                40)];
        [self.maestriaImage setFrame:CGRectMake(20,
                                           60,
                                           self.maestriaLabel.bounds.size.width-10,
                                           180)];
        
        [self.webDetalle setFrame:CGRectMake(20,
                                                260,
                                                self.maestriaLabel.bounds.size.width-10,
                                                180)];
        [self.bordeView setFrame:CGRectMake(0,
                                            0,
                                            10,
                                            self.bordeView.bounds.size.height)];
         
    }
    
    
    //maestriaImage.image =  [UIImage imageNamed:imagen];
    [self setFlickrData:self.loadingImageURLString];
     
}
 
- (void)viewDidUnload
{
    [self setMaestriaImage:nil];
    [self setWebDetalle:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

-(IBAction)done:(id)sender{
    [self dismissModalViewControllerAnimated:YES];
}

-(void)buttonURLPressed:(id)sender { 
    link = url;
    [self performSegueWithIdentifier:@"ShowDiplomadoURL" sender:self];
}

-(void)buttonBrochurePressed:(id)sender { 
    link = brochure;
    [self performSegueWithIdentifier:@"ShowDiplomadoURL" sender:self];
}

-(void)setShareButtonTapped:(id)sender {
    if (sharePicker == nil) {
        self.sharePicker = [[SharePickerViewController alloc]
                            initWithStyle:UITableViewStylePlain];
        sharePickerPopover = [[UIPopoverController alloc] initWithContentViewController:sharePicker];
        
        [sharePicker setDelegate:self];
    }
    [sharePickerPopover presentPopoverFromRect:[sender bounds] inView:sender permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"ShowDiplomadoURL"]) { 
        NavegadorViewController  *destViewController = segue.destinationViewController;
        destViewController.url = link;
    } 
}

- (void)socialSelected:(NSString *)social {

    link =  [NSString stringWithFormat:social , encodeURL( url ), encodeURL( maestriaName )]; 
    [self performSegueWithIdentifier:@"ShowDiplomadoURL" sender:self];
    [self.sharePickerPopover dismissPopoverAnimated:YES];
}

-(void) setFlickrData:(NSString*) urlPhoto {
      
    
    self.imageLoadingOperation = [ApplicationDelegate.postgradoEngine imageAtURL:[NSURL URLWithString:self.loadingImageURLString]
        onCompletion:^(UIImage *fetchedImage, NSURL *urlImage, BOOL isInCache) {
            if([self.loadingImageURLString isEqualToString:[urlImage absoluteString]]) {
              if (isInCache) {
                  self.maestriaImage.image = fetchedImage; 
              } else {
                  UIImageView *loadedImageView = [[UIImageView alloc] initWithImage:fetchedImage];
                  loadedImageView.frame = self.maestriaImage.frame;
                  loadedImageView.alpha = 0;
                  //loadedImageView.contentMode = UIViewContentModeCenter;
                  [self.view addSubview:loadedImageView];
                  self.maestriaImage = loadedImageView;
                  
                  [UIView animateWithDuration:0.4
                        animations:^ {
                            loadedImageView.alpha = 1;
                            self.maestriaImage.alpha = 0;
                            
                        }
                        completion:^(BOOL finished) {
                            self.maestriaImage.image = fetchedImage;
                            self.maestriaImage.alpha = 1; 
                    }];
              }
        }
    }];
}



-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    
    [self setLandscapeImages];
    
}

-(void) setLandscapeImages{
    //bool isLandscape = UIDeviceOrientationIsLandscape([[UIDevice currentDevice] orientation]);
    
    self.view.backgroundColor =  [UIColor colorWithPatternImage:[UIImage imageNamed:isLandscape  ? @"bg-page-h.jpg": @"bg-page.png"]];
}

@end
