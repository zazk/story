//
//  YoutubeEngine.h
//  Story
//
//  Created by apple on 8/09/12.
//  Copyright (c) 2012 apple. All rights reserved.
//
 
#import "MKNetworkKit/MKNetworkEngine.h"
#define YOUTUBE_ID @"PLF438222D2BAE909F"
#define YOUTUBE_URL @"feeds/api/playlists/%@?v=2&alt=json"
#define YOUTUBE_STRING [NSString stringWithFormat:YOUTUBE_URL,YOUTUBE_ID]

@interface YoutubeEngine : MKNetworkEngine

typedef void (^YoutubeImagesResponseBlock)(NSMutableArray* imageURLs);
-(void) onCompletion:(YoutubeImagesResponseBlock) imageURLBlock
             onError:(MKNKErrorBlock) errorBlock;
@end
