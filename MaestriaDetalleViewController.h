//
//  MaestriaDetalleViewController.h
//  Story
//
//  Created by apple on 8/08/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavegadorViewController.h"
#import "SharePickerViewController.h"
#import "UIColor-Expanded.h"
#import "Common.h" 
#import "Constants.h"

@interface MaestriaDetalleViewController : UIViewController <SharePickerDelegate>{ 
}
-(IBAction)done:(id)sender;
@property (strong,nonatomic) NSString *detalle;
@property (nonatomic, strong) NSString *maestriaName;
@property (nonatomic, strong) NSString *colorHEX; 
@property (strong, nonatomic) IBOutlet UIView *bordeView;
@property (strong, nonatomic) IBOutlet UIWebView *webVideo; 
@property (strong, nonatomic) IBOutlet UIWebView *webDetailText;
@property (strong, nonatomic) IBOutlet UILabel *maestriaLabel; 
@property (nonatomic,strong) NSString *url;
@property (nonatomic,strong) NSString *link;
@property (nonatomic,strong) NSString *brochure;
@property (nonatomic,strong) NSString *urlvideo;
- (void)embedYouTube:(NSString *)urlString ;
@property (nonatomic, retain) SharePickerViewController *sharePicker;
@property (nonatomic, retain) UIPopoverController *sharePickerPopover; 
@end
