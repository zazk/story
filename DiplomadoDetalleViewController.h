//
//  DiplomadoDetalleViewController.h
//  Story
//
//  Created by apple on 8/08/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Common.h"
#import "NavegadorViewController.h"
#import "SharePickerViewController.h"

@interface DiplomadoDetalleViewController : UIViewController <SharePickerDelegate>
-(IBAction)done:(id)sender;  
@property (strong, nonatomic) IBOutlet UILabel *maestriaLabel;
@property (strong, nonatomic) IBOutlet UIWebView *webDetalle;
@property (strong, nonatomic) IBOutlet UIImageView *maestriaImage;
@property (strong, nonatomic) IBOutlet UIView *bordeView;
@property (strong,nonatomic) NSString *detalle;
@property (nonatomic,strong) NSString *imagen;
@property (nonatomic,strong) NSString *url;
@property (nonatomic,strong) NSString *link;
@property (nonatomic,strong) NSString *brochure;
@property (nonatomic, strong) NSMutableString *maestriaName;
@property (nonatomic, retain) SharePickerViewController *sharePicker;
@property (nonatomic, retain) UIPopoverController *sharePickerPopover;
@property (nonatomic, strong) NSString* loadingImageURLString;
@property (nonatomic, strong) MKNetworkOperation* imageLoadingOperation;
-(void) setFlickrData:(NSString*) urlPhoto;
@end
 