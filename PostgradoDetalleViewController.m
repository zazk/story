//
//  PostgradoDetalleViewController.m
//  Story
//
//  Created by apple on 23/08/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import "PostgradoDetalleViewController.h"
#import "Constants.h"

@interface PostgradoDetalleViewController ()

@end

@implementation PostgradoDetalleViewController
@synthesize lblTitulo;
@synthesize webDetalle;
@synthesize titulo,html,enlace;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) viewDidAppear:(BOOL)animated{
    
    [self setLandscapeImages];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    lblTitulo.text = titulo;
    self.webDetalle.delegate = self;
    [webDetalle setBackgroundColor:[UIColor clearColor]];
    [webDetalle setOpaque:NO];
    [webDetalle loadHTMLString:[NSString stringWithFormat:kBaseHTML, html] baseURL:nil];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        
        [self.lblTitulo setFrame:CGRectMake(10,
                                            10,
                                            300,
                                            40)];
        self.lblTitulo.font = [UIFont boldSystemFontOfSize:19.0];
        
    }
    [self setLandscapeImages];
}

- (void)viewDidUnload
{
    [self setLblTitulo:nil];
    [self setWebDetalle:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    
    [self setLandscapeImages];
    
}

-(void) setLandscapeImages{
    //bool isLandscape = UIDeviceOrientationIsLandscape([[UIDevice currentDevice] orientation]);
    self.view.backgroundColor =  [UIColor colorWithPatternImage:[UIImage imageNamed:isLandscape  ? @"bg-page-h.jpg": @"bg-page.png"]];
}


-(BOOL) webView:(UIWebView *)inWeb shouldStartLoadWithRequest:(NSURLRequest *)inRequest navigationType:(UIWebViewNavigationType)inType {
    if ( inType == UIWebViewNavigationTypeLinkClicked ) {
            enlace= [[inRequest URL] absoluteString];
            [self performSegueWithIdentifier:@"ShowExternalLink" sender:self];
        return NO;
    }
    
    return YES;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"ShowExternalLink"]) { 
        NavegadorViewController  *destViewController = segue.destinationViewController;
        //destViewController.title = @"Navegador";
        destViewController.url = enlace;
    }
}

- (void)webViewDidFinishLoad:(UIWebView *)theWebView
{
     
}

@end
