//
//  CustomIndicator.m
//  Story
//
//  Created by apple on 7/08/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import "CustomIndicator.h"

@implementation CustomIndicator

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
 */
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    // (x,y) is the tip of the arrow
    //CGFloat x = CGRectGetMaxX(self.bounds) - RIGHT_MARGIN;
    NSLog(@"Repainted Indicator------------");
    CGFloat x = CGRectGetMaxX(self.bounds) - 10;
    CGFloat y = CGRectGetMidY(self.bounds);
    const CGFloat R = 4.5;
    CGContextRef ctxt = UIGraphicsGetCurrentContext();
    CGContextMoveToPoint(ctxt, x-R, y-R);
    CGContextAddLineToPoint(ctxt, x, y);
    CGContextAddLineToPoint(ctxt, x-R, y+R);
    CGContextSetLineCap(ctxt, kCGLineCapSquare);
    CGContextSetLineJoin(ctxt, kCGLineJoinMiter);
    CGContextSetLineWidth(ctxt, 3);
    // If the cell is highlighted (blue background) draw in white; otherwise gray
    //if (CONTROL_IS_HIGHLIGHTED) {
        CGContextSetRGBStrokeColor(ctxt, 0, 0, 0, 1);
    //} else {
    //    CGContextSetRGBStrokeColor(ctxt, 0.5, 0.5, 0.5, 1);
    //}
    CGContextStrokePath(ctxt);
}

@end
