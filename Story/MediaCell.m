//
//  MediaCell.m
//  Story
//
//  Created by apple on 6/09/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import "MediaCell.h"

@implementation MediaCell
@synthesize titleLabel = titleLabel_;
@synthesize authorNameLabel = authorNameLabel_;
@synthesize thumbnailImage = thumbnailImage_;
@synthesize loadingImageURLString = loadingImageURLString_;
@synthesize imageLoadingOperation = imageLoadingOperation_;

//===========================================================
// + (BOOL)automaticallyNotifiesObserversForKey:
//
//===========================================================
+ (BOOL)automaticallyNotifiesObserversForKey: (NSString *)theKey
{
    BOOL automatic;
    
    if ([theKey isEqualToString:@"thumbnailImage"]) {
        automatic = NO;
    } else {
        automatic = [super automaticallyNotifiesObserversForKey:theKey];
    }
    
    return automatic;
}
-(void) prepareForReuse {
    
    self.thumbnailImage.image = nil;
    [self.imageLoadingOperation cancel];
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) setFlickrData:(NSDictionary*) thisFlickrImage {
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        self.titleLabel.font = [UIFont boldSystemFontOfSize:12.0];
        self.authorNameLabel.font = [UIFont boldSystemFontOfSize:10.0];
        [self.authorNameLabel setFrame:CGRectMake(70,
                                             40,
                                             180,
                                             40)];
    }
    
    self.titleLabel.text = [[thisFlickrImage objectForKey:@"title"] objectForKey:@"_content"];
	self.authorNameLabel.text = [[thisFlickrImage objectForKey:@"description"] objectForKey:@"_content"];
    self.loadingImageURLString =
    [NSString stringWithFormat:@"http://farm%@.static.flickr.com/%@/%@_%@_q.jpg",
     [thisFlickrImage objectForKey:@"farm"], [thisFlickrImage objectForKey:@"server"],
     [thisFlickrImage objectForKey:@"primary"], [thisFlickrImage objectForKey:@"secret"]];
    //NSLog(@"Load Complete URL: %@",self.loadingImageURLString);
    
    self.imageLoadingOperation = [ApplicationDelegate.flickrEngine imageAtURL:[NSURL URLWithString:self.loadingImageURLString]
        onCompletion:^(UIImage *fetchedImage, NSURL *url, BOOL isInCache) {                                                   
            if([self.loadingImageURLString isEqualToString:[url absoluteString]]) {
                                                                         
                if (isInCache) {
                    self.thumbnailImage.image = fetchedImage;
                } else {
                    UIImageView *loadedImageView = [[UIImageView alloc] initWithImage:fetchedImage];
                    loadedImageView.frame = self.thumbnailImage.frame;
                    loadedImageView.alpha = 0;
                    [self.contentView addSubview:loadedImageView];
                                                                             
                    [UIView animateWithDuration:0.4
                             animations:^ {
                                 loadedImageView.alpha = 1;
                                 self.thumbnailImage.alpha = 0;
                             }
                             completion:^(BOOL finished) {
                                self.thumbnailImage.image = fetchedImage;
                                self.thumbnailImage.alpha = 1;
                                [loadedImageView removeFromSuperview];
                            }];
                }
            }
        }];
}


@end
