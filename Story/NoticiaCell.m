//
//  NoticiaCell.m
//  Story
//
//  Created by apple on 3/09/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import "NoticiaCell.h"

@implementation NoticiaCell
@synthesize imgNoticia;
@synthesize lblTitulo;
@synthesize lblDetalle;
@synthesize loadingImageURLString;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) setNoticiaData:(NSDictionary*) thisNoticiaImage {
     
    self.lblTitulo.text = [thisNoticiaImage objectForKey:@"titulo"];
    self.lblDetalle.text = [thisNoticiaImage objectForKey:@"resumenApp"]; 

    self.loadingImageURLString = [thisNoticiaImage objectForKey:@"miniaturaApp"];
    //NSLog(@"Load Complete URL: %@",self.loadingImageURLString);
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) { 
        self.lblTitulo.font = [UIFont boldSystemFontOfSize:12.0];
        self.lblDetalle.font = [UIFont boldSystemFontOfSize:10.0];
        [self.imgNoticia setFrame:CGRectMake(10,
                                             10,
                                             80,
                                             60)];
        [self.lblTitulo setFrame:CGRectMake(100,
                                             10,
                                             180,
                                             30)];
        [self.lblDetalle setFrame:CGRectMake(100,
                                           25,
                                           180,	
                                           60)];
    }
    self.imageLoadingOperation = [ApplicationDelegate.noticiasEngine imageAtURL:[NSURL URLWithString:self.loadingImageURLString]
        onCompletion:^(UIImage *fetchedImage, NSURL *url, BOOL isInCache) {
                  if([self.loadingImageURLString isEqualToString:[url absoluteString]]) {
              if (isInCache) {
                  self.imgNoticia.image = fetchedImage;
              } else {
                  UIImageView *loadedImageView = [[UIImageView alloc] initWithImage:fetchedImage];
                  loadedImageView.frame = self.imgNoticia.frame;
                  loadedImageView.alpha = 0;
                  [self.contentView addSubview:loadedImageView];
                                                                             
                  [UIView animateWithDuration:0.4
                        animations:^ {
                            loadedImageView.alpha = 1;
                            self.imgNoticia.alpha = 0;
                        }
                        completion:^(BOOL finished) {
                            self.imgNoticia.image = fetchedImage;
                            self.imgNoticia.alpha = 1; 
                    }];
              }
        }
    }];
}

@end
