//
//  SetsViewController.m
//  Story
//
//  Created by apple on 6/09/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import "SetsViewController.h"
  
@implementation SetsViewController

@synthesize sets;
@synthesize menuView;

 
- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

-(void)viewDidAppear:(BOOL)animated{
    [self setLandscapeImages];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [ApplicationDelegate.flickrEngine imagesForKey:@"photosets" subkey:@"photoset"   onCompletion:^(NSMutableArray* images) {
        
        self.sets = images; 
        [menuView reloadData];
    }
    onError:^(NSError* error) {
                                               
    }];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        
        self.lblTitulo.font = [UIFont boldSystemFontOfSize:18.0];
        [self.lblTitulo setFrame:CGRectMake(20,
                                            20,
                                            280,
                                            20)];
        [self.menuView setFrame:CGRectMake(20,
                                           40,
                                           280,
                                           400)];
        
    }
	// Do any additional setup after loading the view.
    self.view.backgroundColor =  [UIColor colorWithPatternImage:
                                  [UIImage imageNamed:@"bg-page.png"]];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    [self setLandscapeImages];
    
}

- (void)viewDidUnload
{ 
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setLandscapeImages];
}
 

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}


#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        return   80  ;
    }
    return   170  ;

}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}
 
- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [ sets count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	MediaCell *cell = (MediaCell*)[tableView dequeueReusableCellWithIdentifier:@"FlickrCell"];
    
    NSDictionary *thisFlickrImage = [self.sets objectAtIndex:indexPath.row]; 
    
    // code from Stanford Tutorial 
    [cell setFlickrData:thisFlickrImage];
    
	return cell;
}

 


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller. 
        [self performSegueWithIdentifier:@"ShowSet" sender:self];
}
 

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if ([segue.identifier isEqualToString:@"ShowSet"]) {
        
        NSIndexPath *indexPath = [self.menuView indexPathForSelectedRow];
        FlickrViewController  *destViewController = segue.destinationViewController;
        destViewController.title = [ [ [self.sets objectAtIndex:indexPath.row] objectForKey:@"title"] objectForKey:@"_content"] ;
        
        destViewController.count =  [[ [self.sets objectAtIndex:indexPath.row] objectForKey:@"photos"] intValue ];
        destViewController.idset =  [ [self.sets objectAtIndex:indexPath.row] objectForKey:@"id"] ;
        
    }
}

-(void) setLandscapeImages{
    //bool isLandscape = UIDeviceOrientationIsLandscape([[UIDevice currentDevice] orientation]);
    
    self.view.backgroundColor =  [UIColor colorWithPatternImage:[UIImage imageNamed:isLandscape  ? @"bg-page-h.jpg": @"bg-page.png"]]; 
}

@end
