//
//  BoletinViewController.h
//  Story
//
//  Created by apple on 31/08/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BoletinCell.h"
#import "NavegadorViewController.h"
#import "CustomCellBackground.h"

@interface BoletinViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (strong, nonatomic) IBOutlet UITableView *menuView;
@property (strong, nonatomic) IBOutlet UILabel *lblTitulo;

@end
