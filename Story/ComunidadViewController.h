//
//  ComunidadViewController.h
//  Story
//
//  Created by apple on 24/08/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavegadorViewController.h"
#import "CustomCellBackground.h"
#import "UIColor-Expanded.h"
#import "Common.h"
#import "BlogCell.h"

@interface ComunidadViewController : UIViewController <UITableViewDataSource,UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *menuView;
@property (strong, nonatomic) IBOutlet UITableView *socialView;
@property (strong, nonatomic) IBOutlet UIView *imgRule;
@property (nonatomic,strong) NSString *link; 
-(void)buttonURLPressed:(id)sender withLink:(NSString *)url;
@property (strong, nonatomic) IBOutlet UILabel *lblTitulo;
@end
