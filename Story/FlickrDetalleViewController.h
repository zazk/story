//
//  FlickrDetalleViewController.h
//  Story
//
//  Created by apple on 5/09/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FlickrDetalleViewController : UIViewController
 
@property (nonatomic, strong) NSString* loadingImageURLString;
@property (nonatomic, strong) MKNetworkOperation* imageLoadingOperation;
@property (strong, nonatomic) IBOutlet UIImageView *thumbnailImage;
-(void) setFlickrData:(NSString*) urlPhoto;
@end
