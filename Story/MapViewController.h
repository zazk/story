//
//  MapViewController.h
//  Story
//
//  Created by zazk on 17/09/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "UPAnnotation.h"

@interface MapViewController : UIViewController<MKMapViewDelegate>

@property (strong, nonatomic) IBOutlet MKMapView *mapViewer;
@property (strong, nonatomic) UPAnnotation *upAnnotation;

@end
