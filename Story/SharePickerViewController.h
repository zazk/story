//
//  SharePickerViewController.h
//  Story
//
//  Created by apple on 10/09/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import <UIKit/UIKit.h> 

@protocol SharePickerDelegate
- (void)socialSelected:(NSString *)social;
@end


@interface SharePickerViewController : UITableViewController {
}

@property (nonatomic, retain) NSMutableArray *colors,*links;
@property (nonatomic, assign) id<SharePickerDelegate> delegate;

@end