//
//  VideoCell.m
//  Story
//
//  Created by zazk on 11/09/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import "VideoCell.h"

@implementation VideoCell

@synthesize titleLabel = titleLabel_;
@synthesize idvideo = idvideo_;
@synthesize authorNameLabel = authorNameLabel_;
@synthesize thumbnailImage = thumbnailImage_;
@synthesize loadingImageURLString = loadingImageURLString_;
@synthesize imageLoadingOperation = imageLoadingOperation_;

//===========================================================
// + (BOOL)automaticallyNotifiesObserversForKey:
//
//===========================================================
+ (BOOL)automaticallyNotifiesObserversForKey: (NSString *)theKey
{
    BOOL automatic;
    
    if ([theKey isEqualToString:@"thumbnailImage"]) {
        automatic = NO;
    } else {
        automatic = [super automaticallyNotifiesObserversForKey:theKey];
    }
    
    return automatic;
}
-(void) prepareForReuse {
    
    self.thumbnailImage.image = nil;
    [self.imageLoadingOperation cancel];
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) setYoutubeData:(NSDictionary*) thisYoutubeImage {
    
    self.idvideo = [ [ [thisYoutubeImage objectForKey:@"media$group"] objectForKey:@"yt$videoid" ] objectForKey:@"$t" ];
    self.titleLabel.text = [ [ [thisYoutubeImage objectForKey:@"media$group"] objectForKey:@"media$title" ] objectForKey:@"$t" ];
	self.authorNameLabel.text = [ [ [thisYoutubeImage objectForKey:@"media$group"] objectForKey:@"media$description" ] objectForKey:@"$t" ];
    self.loadingImageURLString = 
        [NSString stringWithFormat:@"http://i.ytimg.com/vi/%@/mqdefault.jpg",
             [ [ [thisYoutubeImage objectForKey:@"media$group"] objectForKey:@"yt$videoid" ] objectForKey:@"$t" ] ]; 
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        self.titleLabel.font = [UIFont boldSystemFontOfSize:12.0];
        self.authorNameLabel.font = [UIFont boldSystemFontOfSize:10.0];
        [self.authorNameLabel setFrame:CGRectMake(70,
                                                  40,
                                                  180,
                                                  40)];
    }
    self.imageLoadingOperation = [ApplicationDelegate.flickrEngine imageAtURL:[NSURL URLWithString:self.loadingImageURLString]
        onCompletion:^(UIImage *fetchedImage, NSURL *url, BOOL isInCache) {                                                   
            if([self.loadingImageURLString isEqualToString:[url absoluteString]]) {
                                                                         
                if (isInCache) {
                    self.thumbnailImage.image = fetchedImage;
                } else {
                    UIImageView *loadedImageView = [[UIImageView alloc] initWithImage:fetchedImage];
                    loadedImageView.frame = self.thumbnailImage.frame;
                    loadedImageView.alpha = 0;
                    [self.contentView addSubview:loadedImageView];
                                                                             
                    [UIView animateWithDuration:0.4
                             animations:^ {
                                 loadedImageView.alpha = 1;
                                 self.thumbnailImage.alpha = 0;
                             }
                             completion:^(BOOL finished) {
                                self.thumbnailImage.image = fetchedImage;
                                self.thumbnailImage.alpha = 1;
                                [loadedImageView removeFromSuperview];
                            }];
                }
            }
        }];
}

@end
