//
//  SetsViewController.h
//  Story
//
//  Created by apple on 6/09/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MediaCell.h"
#import "FlickrViewController.h"

@interface SetsViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property (strong, nonatomic) NSMutableArray *sets;
@property (strong, nonatomic) IBOutlet UITableView *menuView;
@property (strong, nonatomic) IBOutlet UILabel *lblTitulo;

@end
