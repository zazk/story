//
//  FlickrViewController.h
//  Story
//
//  Created by apple on 5/09/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "GMGridView/GMGridView.h"
#import "PhotoCell.h"
#import "FlickrDetalleViewController.h"
#import "MWPhotoBrowser.h"

@interface FlickrViewController : UIViewController<GMGridViewDataSource, GMGridViewSortingDelegate, GMGridViewTransformationDelegate,
    GMGridViewActionDelegate,MWPhotoBrowserDelegate>
{  
}


@property (nonatomic, assign) int count,lastTappedIndex;
@property (strong, nonatomic) NSMutableArray *photos;
@property (strong, nonatomic) NSMutableArray *stack;
@property (strong, nonatomic) NSString *idset;

- (void)addMoreItem;
- (void)removeItem;
- (void)refreshItem;
- (void)presentOptions:(UIBarButtonItem *)barButton;
- (void)optionsDoneAction;
- (void)dataSetChange:(UISegmentedControl *)control;

@end
