//
//  EventoDetalleViewController.m
//  Story
//
//  Created by zazk on 15/09/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import "EventoDetalleViewController.h"

@interface EventoDetalleViewController ()

@end

@implementation EventoDetalleViewController
@synthesize lblTitulo;
@synthesize imgEvento;
@synthesize webDetalle;
@synthesize lblDia;
@synthesize lblMes;
@synthesize loadingImageURLString,imageLoadingOperation,webContentString,dia,mes,titulo;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewDidAppear:(BOOL)animated{
    [self setLandscapeImages];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.view.backgroundColor =  [UIColor colorWithPatternImage:
                                  [UIImage imageNamed:@"bg-page.png"]];
    
    lblDia.text = dia;
    lblMes.text = mes;
    lblTitulo.text = titulo;
    
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        
        self.lblTitulo.font = [UIFont boldSystemFontOfSize:16.0];
        self.lblDia.font = [UIFont boldSystemFontOfSize:24.0];
        [self.imgEvento setFrame:CGRectMake(20,
                                            80,
                                            280,
                                            120)];
        [self.viewDate setFrame:CGRectMake(250,
                                            10,
                                            50,
                                            60)];
        [self.lblTitulo setFrame:CGRectMake(20,
                                                10,
                                                230,
                                            40)];  
        [self.webDetalle setFrame:CGRectMake(20,
                                                220,
                                                280,
                                                200)];
         
    }
    
    [self setFlickrData:self.loadingImageURLString];
    [webDetalle setBackgroundColor:[UIColor clearColor]];
    [webDetalle setOpaque:NO];
    [webDetalle loadHTMLString:[NSString stringWithFormat:kBaseHTML, webContentString] baseURL:nil];
    [self setLandscapeImages];
    
}

- (void)viewDidUnload
{
    [self setLblTitulo:nil];
    [self setImgEvento:nil];
    [self setWebDetalle:nil];
    [self setLblDia:nil];
    [self setLblMes:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


-(void) setFlickrData:(NSString*) urlPhoto { 
    
    self.imageLoadingOperation = [ApplicationDelegate.eventosEngine imageAtURL:[NSURL URLWithString:self.loadingImageURLString]
        onCompletion:^(UIImage *fetchedImage, NSURL *url, BOOL isInCache) {
            if([self.loadingImageURLString isEqualToString:[url absoluteString]]) {
              if (isInCache) {
                  self.imgEvento.image = fetchedImage;
              } else {
                  UIImageView *loadedImageView = [[UIImageView alloc] initWithImage:fetchedImage];
                  loadedImageView.frame = self.imgEvento.frame;
                  loadedImageView.alpha = 0;
                  //loadedImageView.contentMode = UIControlContentVerticalAlignmentFill;
                  [self.view addSubview:loadedImageView];
                  self.imgEvento = loadedImageView;                                                           
                  [UIView animateWithDuration:0.4
                        animations:^ {
                            loadedImageView.alpha = 1;
                            self.imgEvento.alpha = 0;
                        }
                        completion:^(BOOL finished) {
                            self.imgEvento.image = fetchedImage;
                            self.imgEvento.alpha = 1; 
                    }];
              }
        }
    }];
}

-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    
    [self setLandscapeImages];
    
}

-(void) setLandscapeImages{
    //bool isLandscape = UIDeviceOrientationIsLandscape([[UIDevice currentDevice] orientation]);
    
    self.view.backgroundColor =  [UIColor colorWithPatternImage:[UIImage imageNamed:isLandscape  ? @"bg-page-h.jpg": @"bg-page.png"]];
}


@end
