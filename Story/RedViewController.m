//
//  RedViewController.m
//  Story
//
//  Created by apple on 1/09/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import "RedViewController.h"

@interface RedViewController ()

@end

@implementation RedViewController{
    NSMutableString *html,*titulo;
    NSMutableArray *titulos, *htmls;
    NSMutableArray *data;
}
@synthesize btnImpact;
@synthesize btnMentores;
@synthesize btnDesarrollo;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) viewDidAppear:(BOOL)animated{
    
    [self setLandscapeImages];
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    data = [[[Store sharedInstance] myData] objectForKey:@"red"];
    //data = nil;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        
        self.lblTitulo.font= [UIFont boldSystemFontOfSize:18.0];
        [self.lblTitulo setFrame:CGRectMake(10,
                                            10,
                                            320,
                                            20)];
        self.btnImpactLabel.titleLabel.font = self.btnDesarrolloLabel.titleLabel.font  = self.btnMentoresLabel.titleLabel.font = [UIFont boldSystemFontOfSize:16.0];
    }
    if ( data ) {
        NSLog(@"Data Loading in Red View Controller:%@", data);
    }else{
        NSString *path = [[NSBundle mainBundle] pathForResource:@"Static" ofType:@"plist"];
        NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:path];
        titulos = [dict objectForKey:@"Red" ];
        htmls = [dict objectForKey:@"RedDetalle" ];
    }
    [self setLandscapeImages];
}

- (void)viewDidUnload
{
    [self setBtnMentores:nil];
    [self setBtnImpact:nil];
    [self setBtnDesarrollo:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

- (IBAction)showRed:(id)sender { 
    [self addValues:0]; 
}

- (IBAction)showNet:(id)sender { 
    [self addValues:1];
}
 
- (IBAction)showDesarrollo:(id)sender {
    [self addValues:2];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"ShowRed"]) { 
        PostgradoDetalleViewController  *destViewController = segue.destinationViewController;
        destViewController.titulo = titulo;
        destViewController.html = html;
    }
}


- (void)addValues:(int)index {
    if (data) {
        titulo = [[data objectAtIndex:index] objectForKey:@"title"];
        html = [[data objectAtIndex:index] objectForKey:@"content"];
    }else{
        titulo = [titulos objectAtIndex:index];
        html = [htmls objectAtIndex:index];
    }
    NSLog(@"Titulo:%@ ",titulo);
    [self performSegueWithIdentifier:@"ShowRed" sender:self];
}

-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    
    [self setLandscapeImages];
    
}

-(void) setLandscapeImages{
    //bool isLandscape = UIDeviceOrientationIsLandscape([[UIDevice currentDevice] orientation]);
    
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) { 
        
        btnMentores.backgroundColor = [UIColor colorWithPatternImage:[self setBackground:@"img-red-00.jpg"]];
        btnImpact.backgroundColor =[UIColor colorWithPatternImage:[self setBackground:@"img-red-01.jpg"]];
        btnDesarrollo.backgroundColor =[UIColor colorWithPatternImage:[self setBackground:@"img-red-02.jpg"]];
        
    }else{
        
        self.view.backgroundColor =  [UIColor colorWithPatternImage:[UIImage imageNamed:isLandscape  ? @"bg-page-h.jpg": @"bg-page.png"]];
        btnMentores.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:isLandscape  ? @"img-red-00-h.jpg": @"img-red-00.jpg"]];
        btnImpact.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:isLandscape ? @"img-red-01-h.jpg": @"img-red-01.jpg"]];
        btnDesarrollo.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:isLandscape ? @"img-red-02-h.jpg": @"img-red-02.jpg"]];
    }
}

-(UIImage *)  setBackground: (NSString*) image{
    
    UIGraphicsBeginImageContext(btnMentores.bounds.size);
    [[UIImage imageNamed:image] drawInRect:btnMentores.bounds];
    UIImage *bg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return bg;
    
}

@end
