//
//  StoryViewController.m
//  Story
//
//  Created by apple on 5/08/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import "StoryViewController.h"
#import "ProgramasViewController.h"

@interface StoryViewController ()

@end

@implementation StoryViewController{ 
    NSMutableArray *menu;
    NSMutableArray *icons;
    NSMutableArray *targets;
    NSMutableArray *screens;
}

@synthesize menuView;
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    // Find out of the path of Static.plist
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Static" ofType:@"plist"];
    // Load the file content and read the data into the arrays
    NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:path];
    menu = [dict objectForKey:@"Menu"];
    icons = [dict objectForKey:@"Icons"];
    screens = [dict objectForKey:@"Screens"]; 
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(receivedRotate:) name: UIDeviceOrientationDidChangeNotification object: nil];
    self.menuView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:.4];
    
    // Conditional iOS Device
    NSString *logo = @"logo.png";
    int LOGO_WIDTH =240;
    int LOGO_HEIGTH =238;
    if   (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        // Place iPhone/iPod specific code here...			
        logo = @"logo-ipod.png";
        LOGO_WIDTH =120;
        LOGO_HEIGTH =119; 
        
        [self.menuView setFrame:CGRectMake(0,
                                            0,
                                            150,
                                            menuView.bounds.size.height)];
    }
    
    // Add Customize Header view
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, LOGO_WIDTH, LOGO_HEIGTH)];
    headerView.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.8];
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 0, LOGO_WIDTH, LOGO_HEIGTH)];
    imageView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:logo]];
    [headerView addSubview:imageView]; 
    self.menuView.tableHeaderView = headerView;
    //self.menuView.superview.
    
    if( [[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0f ) {
        float statusBarHeight = [[UIApplication sharedApplication] statusBarFrame].size.height;
        for( UIView *v in [self.view subviews] ) {
            CGRect rect = v.frame;
            rect.origin.y += statusBarHeight;
            v.frame = rect;
        }
    }
    
    [self setLandscapeImages];
     
    [ApplicationDelegate.postgradoEngine
     onCompletion:^(NSMutableDictionary* images) {
         [[Store sharedInstance] setMyData:images]; 

    }
    onError:^(NSError* error) {
                                               
    }];
    
} 

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

-(void) receivedRotate: (NSNotification*) notification
{
    
    [self setLandscapeImages];
    [self.menuView reloadData];
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [ menu count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *menuID = @"MenuItem";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:menuID];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:menuID];
    }  
    cell.textLabel.text = [menu objectAtIndex:indexPath.row];
    cell.textLabel.textColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:[screens objectAtIndex:indexPath.row] sender:self];
    
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    cell.backgroundColor= [UIColor clearColor];
    //cell.textLabel.backgroundColor = [UIColor clearColor];
    int FONT_SIZE = 20;
    if   (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        FONT_SIZE = 13;
    }
    cell.textLabel.font = [UIFont boldSystemFontOfSize:FONT_SIZE];
    cell.imageView.image = [UIImage imageNamed:[icons objectAtIndex:indexPath.row] ];
    cell.imageView.frame = CGRectMake(0,0,132,132);
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    int ROW_HEIGHT = ( UIDeviceOrientationIsLandscape([[UIDevice currentDevice] orientation] )? 50: 75 );
    if   (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        // Place iPhone/iPod specific code here...
        ROW_HEIGHT = 35;
    }
     
    return ROW_HEIGHT; 
}


-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    
    [self setLandscapeImages];
}
- (void) viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [super viewWillAppear:animated]; 
     
    [self setLandscapeImages];
}

- (void) viewWillDisappear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillDisappear:animated]; 
}

-(void) setLandscapeImages{
    NSString *IMAGEN = (isLandscape?
                        @"bg-home-h.jpg": @"bg-home.jpg");
    
    if   (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        IMAGEN = @"bg-home-iphone.png"; 
    }
    
    self.view.backgroundColor =  [UIColor colorWithPatternImage:[UIImage imageNamed:IMAGEN]];
}
@end
