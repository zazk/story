//
//  MaestriasViewController.m
//  Story
//
//  Created by apple on 8/08/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import "MaestriasViewController.h"
#import "MaestriaDetalleViewController.h"
#import "CustomCellBackground.h"
#import "Common.h"

@interface MaestriasViewController ()

@end

@implementation MaestriasViewController{
    NSMutableArray *menu,*detalles, *colores, *videos,*urls,*brochures;
    NSMutableArray *data;
}

@synthesize menuView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad 
{
    [super viewDidLoad];
	// Do any additional setup after loading the view. 
    menuView.backgroundColor =  [UIColor colorWithPatternImage: [UIImage imageNamed:@"bg-page.png"]];
    
    data = [[[Store sharedInstance] myData] objectForKey:@"maestrias"];
    //data = nil;
    if ( data ) {
        NSLog(@"Data Loading in PostGrado View Controller:%@", data);
    }else{  
        NSString *path = [[NSBundle mainBundle]
                          pathForResource:@"Static" ofType:@"plist"];
        NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:path];
        menu = [dict objectForKey:@"Maestrias" ];
        detalles = [dict objectForKey:@"MaestriasDetalle" ];
        colores = [dict objectForKey:@"MaestriasColor" ];
        videos = [dict objectForKey:@"MaestriasVideos" ];
        urls = [dict objectForKey:@"MaestriasURL" ];
        brochures = [dict objectForKey:@"MaestriasBrochure" ];
    }
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *menuID = @"MaestriasItem";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:menuID];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:menuID ]; 
    }
    cell.backgroundView = [[CustomCellBackground alloc] init]  ;
    cell.textLabel.backgroundColor = [ UIColor clearColor];
    cell.textLabel.numberOfLines = 2;
    cell.textLabel.textColor = [UIColor colorWithRed:70/255.0 green:70/255.0 blue:70/255.0 alpha:1];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) { 
        cell.textLabel.font = [UIFont boldSystemFontOfSize:15.0]; 
    }
    //UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:replace(@"indicator-", indexPath.row)]];
    cell.imageView.image =  [UIImage imageNamed:@"img-12" ];
    //cell.accessoryView = imageView;
    cell.imageView.contentMode = UITableViewCellStyleSubtitle;
    cell.contentView.superview.backgroundColor = [UIColor redColor];
    //[cell addSubview:view];
    
    if (data) {
        int HEIGHT_COLOR = 64;
        int WIDTH_COLOR = 8.0;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) { 
            HEIGHT_COLOR = 50;
            WIDTH_COLOR = 5;
        }
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, WIDTH_COLOR, HEIGHT_COLOR )];
        view.backgroundColor = [UIColor colorWithHexString:[[data objectAtIndex:indexPath.row] objectForKey:@"color"]];
        [cell.imageView addSubview:view];
        cell.textLabel.text = [[data objectAtIndex:indexPath.row] objectForKey:@"title"];
        // set selection color
        
        //iOS7
        if( [[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0f ) {
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            [cell.textLabel setHighlightedTextColor: [UIColor colorWithHexString:[[data objectAtIndex:indexPath.row] objectForKey:@"color"]]];
        }
        // for all your labels
        //--
        
        UIView *myBackView = [[UIView alloc] initWithFrame:cell.frame];
        myBackView.backgroundColor = view.backgroundColor;
        cell.selectedBackgroundView = myBackView;
    }else{
        cell.textLabel.text = [menu objectAtIndex:indexPath.row]; 
    }
    
    
    return cell;
}
/*
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    cell.contentView.backgroundColor = [UIColor colorWithHexString:[[data objectAtIndex:indexPath.row] objectForKey:@"color"]];
    
}
 */

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
     
    if (data) {
        return [data count];
    }
    return [ menu count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) { 
        return   50  ; 
    }
    return   80  ;
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"ShowMaestriaDetalle"]) {
        NSIndexPath *indexPath = [self.menuView indexPathForSelectedRow];
        MaestriaDetalleViewController  *destViewController = segue.destinationViewController;
        
        if (data) {
            NSDictionary *row = [data objectAtIndex:indexPath.row];
            destViewController.maestriaName = [row objectForKey:@"title"];
            destViewController.detalle = [row objectForKey:@"content"];
            destViewController.colorHEX = [row objectForKey:@"color"];
            destViewController.urlvideo = [row objectForKey:@"media"];
            destViewController.url =[row objectForKey:@"url"];
            destViewController.brochure =[row objectForKey:@"brochure"];
        }else{
            destViewController.maestriaName = [menu objectAtIndex:indexPath.row];
            destViewController.detalle = [detalles objectAtIndex:indexPath.row];
            destViewController.colorHEX = [colores objectAtIndex:indexPath.row];
            destViewController.urlvideo = [videos objectAtIndex:indexPath.row];
            destViewController.url =[urls objectAtIndex:indexPath.row];
            destViewController.brochure =[brochures objectAtIndex:indexPath.row];
        }
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

-(IBAction)done:(id)sender{
    [self dismissModalViewControllerAnimated:YES];
}

@end
