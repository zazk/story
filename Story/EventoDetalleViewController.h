//
//  EventoDetalleViewController.h
//  Story
//
//  Created by zazk on 15/09/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"

@interface EventoDetalleViewController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *lblTitulo;
@property (strong, nonatomic) IBOutlet UIImageView *imgEvento;
@property (strong, nonatomic) IBOutlet UIWebView *webDetalle;
@property (strong, nonatomic) IBOutlet UILabel *lblDia;
@property (strong, nonatomic) IBOutlet UILabel *lblMes;
@property (strong, nonatomic) IBOutlet UIView *viewDate;


@property (strong,nonatomic) NSString *loadingImageURLString;
@property (strong,nonatomic) NSString *webContentString;
@property (strong,nonatomic) NSString *titulo;
@property (strong,nonatomic) NSString *mes;
@property (strong,nonatomic) NSString *dia;
@property (nonatomic,strong) MKNetworkOperation* imageLoadingOperation;

-(void) setFlickrData:(NSString*) urlPhoto;
@end
