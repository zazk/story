//
//  RedViewController.h
//  Story
//
//  Created by apple on 1/09/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PostgradoDetalleViewController.h"

@interface RedViewController : UIViewController
- (IBAction)showRed:(id)sender; 
- (IBAction)showNet:(id)sender;
- (IBAction)showDesarrollo:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *lblTitulo;
@property (strong, nonatomic) IBOutlet UIButton *btnMentores;
@property (strong, nonatomic) IBOutlet UIButton *btnImpact;
@property (strong, nonatomic) IBOutlet UIButton *btnDesarrollo;
@property (strong, nonatomic) IBOutlet UIButton *btnMentoresLabel;
@property (strong, nonatomic) IBOutlet UIButton *btnImpactLabel;
@property (strong, nonatomic) IBOutlet UIButton *btnDesarrolloLabel;

@end
