//
//  NoticiasDetalleViewController.m
//  Story
//
//  Created by apple on 3/09/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import "NoticiasDetalleViewController.h"

@interface NoticiasDetalleViewController ()

@end

@implementation NoticiasDetalleViewController{
    UIButton *btn;
}
@synthesize lblTitulo;
@synthesize lblFecha;
@synthesize imgNoticia; 
@synthesize webDetalle;
@synthesize link,url,detalle,titulo,galeria,imagen,enlace,
    sharePicker,sharePickerPopover,fecha;
@synthesize imageLoadingOperation;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewDidAppear:(BOOL)animated{
    [self setLandscapeImages];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        
        self.lblTitulo.font = [UIFont boldSystemFontOfSize:18.0];
        [self.lblTitulo setFrame:CGRectMake(20,
                                            10,
                                            280,
                                            40)];
        self.lblFecha.font = [UIFont boldSystemFontOfSize:12.0];
        [self.lblFecha setFrame:CGRectMake(20,
                                            50,
                                            280,
                                           20)];
        [self.imgNoticia setFrame:CGRectMake(20,
                                           80,
                                           280,
                                             130)];
        [self.webDetalle setFrame:CGRectMake(20,
                                             220,
                                             280,
                                             220)];
        
    }
    
    self.webDetalle.delegate = self;
    lblTitulo.text = titulo;
    lblFecha.text = fecha;
    [self setFlickrData:self.imagen];
    [webDetalle setBackgroundColor:[UIColor clearColor]];
    [webDetalle setOpaque:NO];
    
    
    NSLog(@"Data Loading in Noticia:%@", detalle);
    
    [webDetalle loadHTMLString:[NSString stringWithFormat:kBaseHTML, detalle] baseURL:nil];
      
    btn = addButton(self,@"Compartir",CGRectMake(40, 500, 150, 34),
                    @selector(setShareButtonTapped:) );
     

    [self setLandscapeImages];
 
}
 

- (void)viewDidUnload
{
    [self setLblTitulo:nil];
    [self setImgNoticia:nil]; 
    [self setWebDetalle:nil];
    [self setLblFecha:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

-(void)buttonGaleriaPressed:(id)sender {
    link = galeria;
    [self performSegueWithIdentifier:@"ShowNoticiaGaleria" sender:self];
}


-(void)setShareButtonTapped:(id)sender {
    if (sharePicker == nil) {
        self.sharePicker = [[SharePickerViewController alloc]
                            initWithStyle:UITableViewStylePlain];
        sharePickerPopover = [[UIPopoverController alloc] initWithContentViewController:sharePicker];
        
        [sharePicker setDelegate:self];
    }
    [sharePickerPopover presentPopoverFromRect:[sender bounds] inView:sender permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"ShowNoticiaGaleria"]) { 
        NavegadorViewController  *destViewController = segue.destinationViewController;
        destViewController.url = link;
    }
}

-(void) setFlickrData:(NSString*) urlPhoto {
    
    self.imageLoadingOperation = [ApplicationDelegate.noticiasEngine imageAtURL:[NSURL URLWithString:self.imagen]
        onCompletion:^(UIImage *fetchedImage, NSURL *urlImage, BOOL isInCache) {
            if([self.imagen isEqualToString:[urlImage absoluteString]]) {
              if (isInCache) {
                  self.imgNoticia.image = fetchedImage;
              } else {
                  UIImageView *loadedImageView = [[UIImageView alloc] initWithImage:fetchedImage];
                  loadedImageView.frame = self.imgNoticia.frame;
                  loadedImageView.alpha = 0;
                  loadedImageView.contentMode = UIControlContentVerticalAlignmentFill;
                  [self.view addSubview:loadedImageView];
                  self.imgNoticia = loadedImageView;                                                           
                  [UIView animateWithDuration:0.4
                        animations:^ {
                            loadedImageView.alpha = 1;
                            self.imgNoticia.alpha = 0;
                        }
                        completion:^(BOOL finished) {
                            self.imgNoticia.image = fetchedImage;
                            self.imgNoticia.alpha = 1; 
                    }];
              }
        }
    }];
}


- (void)socialSelected:(NSString *)social {
    
    link =  [NSString stringWithFormat:social , encodeURL( url ), encodeURL( titulo )]; 
    [self performSegueWithIdentifier:@"ShowNoticiaGaleria" sender:self];
    [self.sharePickerPopover dismissPopoverAnimated:YES];
}


-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    
    [self setLandscapeImages];
    
}

-(BOOL) webView:(UIWebView *)inWeb shouldStartLoadWithRequest:(NSURLRequest *)inRequest navigationType:(UIWebViewNavigationType)inType {
    if ( inType == UIWebViewNavigationTypeLinkClicked ) {
        link= [[inRequest URL] absoluteString];
        [self performSegueWithIdentifier:@"ShowNoticiaGaleria" sender:self];
        return NO;
    }	
    
    return YES;
}

-(void) setLandscapeImages{
    //bool isLandscape = UIDeviceOrientationIsLandscape([[UIDevice currentDevice] orientation]);
    
    self.view.backgroundColor =  [UIColor colorWithPatternImage:[UIImage imageNamed:isLandscape  ? @"bg-page-h.jpg": @"bg-page.png"]];
    if (isLandscape) {
        CGRect buttonFrame = btn.frame;
        buttonFrame.origin.x = 400;
        btn.frame = buttonFrame;
    }else{
        CGRect buttonFrame = btn.frame;
        buttonFrame.origin.x = 40;
        btn.frame = buttonFrame;
    }
}

@end
