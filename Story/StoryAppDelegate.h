//
//  StoryAppDelegate.h
//  Story
//
//  Created by apple on 5/08/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FlickrEngine.h"
#import "YoutubeEngine.h"
#import "PostgradoEngine.h"
#import "EventosEngine.h"
#import "NoticiasEngine.h"

#define ApplicationDelegate ((StoryAppDelegate *)[UIApplication sharedApplication].delegate)
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@interface StoryAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong,nonatomic) NSDictionary *app;

@property (strong, nonatomic) FlickrEngine *flickrEngine;
@property (strong, nonatomic) YoutubeEngine *youtubeEngine;
@property (strong,nonatomic) PostgradoEngine *postgradoEngine;
@property (strong,nonatomic) EventosEngine *eventosEngine;
@property (strong,nonatomic) NoticiasEngine *noticiasEngine;

@end
