//
//  ComunidadViewController.m
//  Story
//
//  Created by apple on 24/08/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import "ComunidadViewController.h"

@interface ComunidadViewController ()

@end

@implementation ComunidadViewController {
    NSMutableArray *menu;
    NSMutableArray *detalles;
    NSMutableArray *urls;
    NSMutableArray *social; 
}

@synthesize menuView;
@synthesize socialView;
@synthesize link;
@synthesize imgRule;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidAppear:(BOOL)animated{
    
    [self setLandscapeImages];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Static" ofType:@"plist"];
    NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:path];
    social = [dict objectForKey:@"Social" ];
    menu = [dict objectForKey:@"Blogger" ];
    detalles = [dict objectForKey:@"BloggerDetalle" ];
    urls = [dict objectForKey:@"BloggerURL" ]; 
    
    
    //Add buttons
    
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        self.lblTitulo.font= [UIFont boldSystemFontOfSize:18.0];
        [self.lblTitulo setFrame:CGRectMake(20,
                                            10,
                                            320,
                                            30)];
        [self.imgRule setFrame:CGRectMake(125,
                                            50,
                                            1,
                                          30)];
        [self.menuView setFrame:CGRectMake(130,
                                          40,
                                          170,
                                          50)];
        
        addButtonWithImage(self,@"btn-facebook",CGRectMake(20, 52, 100, 28),
                           @selector(buttonFacebookPressed:) );
        addButtonWithImage(self,@"btn-twitter",CGRectMake(20, 102, 100, 28),
                           @selector(buttonTwitterPressed:) );
        addButtonWithImage(self,@"btn-youtube",CGRectMake(20, 152, 100, 28),
                           @selector(buttonYoutubePressed:) );
        addButtonWithImage(self,@"btn-linkedin",CGRectMake(20, 202, 100, 28),
                           @selector(buttonLinkedinPressed:) );
        addButtonWithImage(self,@"btn-flickr",CGRectMake(20, 252, 100, 28),
                           @selector(buttonFlickrPressed:) );
        
    }else{
        addButtonWithImage(self,@"btn-facebook",CGRectMake(50, 132, 240, 68),
                           @selector(buttonFacebookPressed:) );
        addButtonWithImage(self,@"btn-twitter",CGRectMake(50, 232, 240, 68),
                           @selector(buttonTwitterPressed:) );
        addButtonWithImage(self,@"btn-youtube",CGRectMake(50, 332, 240, 68),
                           @selector(buttonYoutubePressed:) );
        addButtonWithImage(self,@"btn-linkedin",CGRectMake(50, 432, 240, 68),
                           @selector(buttonLinkedinPressed:) );
        addButtonWithImage(self,@"btn-flickr",CGRectMake(50, 532, 240, 68),
                           @selector(buttonFlickrPressed:) );
    
    }
    [self setLandscapeImages];
}

- (void)viewDidUnload
{
    [self setSocialView:nil];
    [self setImgRule:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}



- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [ menu count];
}


-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *menuID = @"SimpleTableCell";
    BlogCell *cell = (BlogCell *)[tableView dequeueReusableCellWithIdentifier:menuID];
 
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"BlogTableCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    cell.lblName.text = [menu objectAtIndex:indexPath.row];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        [cell.lblName setFrame:CGRectMake(55,
                                           5,
                                           120,
                                           20)];
        cell.lblName.font= [UIFont boldSystemFontOfSize:11.0];
        [cell.lblTitle setFrame:CGRectMake(55,
                                         25,
                                         120,
                                         30)];
        cell.lblTitle.font= [UIFont systemFontOfSize:11.0]; 
        [cell.imgBlogger setFrame:CGRectMake(0,
                                            5,
                                            50,
                                             50)];
        [cell.bgCell setFrame:CGRectMake(5,
                                             5,
                                             200,
                                             50)];
        
    }
    cell.imgBlogger.image = [UIImage imageNamed:[NSString stringWithFormat:@"img-blog-0%d.jpg", indexPath.row] ];
    cell.lblTitle.text= [detalles objectAtIndex:indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setBackgroundColor:[UIColor clearColor]];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        return 70;
        
    }
    return   138  ; } 

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    link = [urls objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:@"ShowBlog" sender:self];
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"ShowBlog"]) {
         
        NavegadorViewController  *destViewController = segue.destinationViewController;
        destViewController.url = link;
    } 
}



-(void)buttonFacebookPressed:(id)sender {
    //UIButton *btn = sender;
    [self buttonURLPressed:sender withLink:@"http://www.facebook.com/postgradopacifico" ];
}

-(void)buttonYoutubePressed:(id)sender {
    //UIButton *btn = sender;
    [self buttonURLPressed:sender withLink:@"http://www.youtube.com/user/EscuelaPostGradoUP" ];
}

-(void)buttonTwitterPressed:(id)sender {
    //UIButton *btn = sender;
    [self buttonURLPressed:sender withLink:@"http://twitter.com/postgradoup" ];
}

-(void)buttonLinkedinPressed:(id)sender {
    //UIButton *btn = sender;
    [self buttonURLPressed:sender withLink:@"http://www.linkedin.com/groups?gid=2004558&goback=.gmp_2004558.gde_2004558_member_112207040" ];
}

-(void)buttonFlickrPressed:(id)sender {
    //UIButton *btn = sender;
    [self buttonURLPressed:sender withLink:@"http://www.flickr.com/photos/postgradoup/" ];
}

-(void)buttonURLPressed:(id)sender withLink:(NSString *)url { 
    link = url;
    [self performSegueWithIdentifier:@"ShowBlog" sender:self];
}

-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    
    [self setLandscapeImages];
    
}
-(void) setLandscapeImages{
    //bool isLandscape = UIDeviceOrientationIsLandscape([[UIDevice currentDevice] orientation]);
    self.view.backgroundColor =  [UIColor colorWithPatternImage:[UIImage imageNamed:isLandscape  ? @"bg-comunidad-h.jpg": @"bg-comunidad.jpg"]];
    if (isLandscape) {
        CGRect buttonFrame = menuView.frame;
        buttonFrame.size.height = 460;
        menuView.frame = buttonFrame;
        
        buttonFrame = imgRule.frame;
        buttonFrame.size.height = 500;
        imgRule.frame = buttonFrame;
    }else{
        CGRect buttonFrame = menuView.frame;
        buttonFrame.size.height = 680;
        menuView.frame = buttonFrame;
        
        buttonFrame = imgRule.frame;
        buttonFrame.size.height = 720;
        imgRule.frame = buttonFrame;
    }
}

 
@end
