//
//  MultimediaViewController.m
//  Story
//
//  Created by apple on 5/09/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import "MultimediaViewController.h"

#define NUMBER_ITEMS_ON_LOAD 50
 

@interface MultimediaViewController ()

@end
 


@implementation MultimediaViewController

@synthesize imgGaleria;
@synthesize imgVideos;
@synthesize btnGaleria;
@synthesize btnVideos;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
 

- (void)viewDidAppear:(BOOL)animated
{
    [self setLandscapeImages];
    [super viewDidAppear:animated];
}
 

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        
        self.btnGaleria.titleLabel.font = self.btnVideos.titleLabel.font = [UIFont boldSystemFontOfSize:18.0]; 
    }
    [self setLandscapeImages];
}


- (void)viewDidUnload
{
    [self setImgGaleria:nil];
    [self setImgVideos:nil];
    [super viewDidUnload]; 
}

 
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
} 

-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    
    [self setLandscapeImages];
    
}

-(void) setLandscapeImages{
    //bool isLandscape = UIDeviceOrientationIsLandscape([[UIDevice currentDevice] orientation]);
    
    self.view.backgroundColor =  [UIColor colorWithPatternImage:[UIImage imageNamed:isLandscape  ? @"bg-page-h.jpg": @"bg-page.png"]];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        imgGaleria.backgroundColor = [UIColor colorWithPatternImage:[self setBackground:@"img-multimedia-00.jpg"]];
        imgVideos.backgroundColor =[UIColor colorWithPatternImage:[self setBackground:@"img-multimedia-01.jpg"]];
        
        
    }else{
        
        imgGaleria.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:isLandscape  ? @"img-multimedia-00-h.jpg": @"img-multimedia-00.jpg"]];
        imgVideos.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:isLandscape ? @"img-multimedia-01-h.jpg": @"img-multimedia-01.jpg"]];
    }
}

-(UIImage *)  setBackground: (NSString*) image{
    
    UIGraphicsBeginImageContext(imgGaleria.bounds.size);
    [[UIImage imageNamed:image] drawInRect:imgGaleria.bounds];
    UIImage *bg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return bg;
    
}

@end
