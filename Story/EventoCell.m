//
//  EventoCell.m
//  Story
//
//  Created by zazk on 14/09/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import "EventoCell.h"

@implementation EventoCell

@synthesize thumbnailImage = thumbnailImage_;
@synthesize lblDia;
@synthesize lblMes;
@synthesize lblTitulo;
@synthesize lblDetalle;
@synthesize viewDate;
@synthesize loadingImageURLString = loadingImageURLString_;
@synthesize imageLoadingOperation = imageLoadingOperation_;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

//===========================================================
// + (BOOL)automaticallyNotifiesObserversForKey:
//
//===========================================================
+ (BOOL)automaticallyNotifiesObserversForKey: (NSString *)theKey
{
    BOOL automatic;
    
    if ([theKey isEqualToString:@"thumbnailImage"]) {
        automatic = NO;
    } else {
        automatic = [super automaticallyNotifiesObserversForKey:theKey];
    }
    
    return automatic;
}
-(void) prepareForReuse {
    
    self.thumbnailImage.image = nil;
    [self.imageLoadingOperation cancel];
}


-(void) setEventoData:(NSDictionary*) thisEventoImage {
    
    self.lblDia.text = [thisEventoImage objectForKey:@"dia"];
    self.lblMes.text = [thisEventoImage objectForKey:@"mes"];
    self.lblTitulo.text = [thisEventoImage objectForKey:@"titulo"];
    self.lblDetalle.text = [thisEventoImage objectForKey:@"resumenApp"];
    self.layer.borderColor = [UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:1.0].CGColor;
    self.layer.borderWidth = 1.0f;

    self.loadingImageURLString = [thisEventoImage objectForKey:@"miniaturaApp"];
    //NSLog(@"Load Complete URL: %@",self.loadingImageURLString);
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        
        self.lblTitulo.font = [UIFont boldSystemFontOfSize:13.0];
        self.lblDetalle.font = [UIFont systemFontOfSize:11.0];
        /*
        [self.bordeView setFrame:CGRectMake(0,
                                            0,
                                            10,
                                            self.bordeView.bounds.size.height)];
         */
    }
    
    self.imageLoadingOperation = [ApplicationDelegate.eventosEngine imageAtURL:[NSURL URLWithString:self.loadingImageURLString]
        onCompletion:^(UIImage *fetchedImage, NSURL *url, BOOL isInCache) {
                  if([self.loadingImageURLString isEqualToString:[url absoluteString]]) {
              if (isInCache) {
                  self.thumbnailImage.image = fetchedImage;
              } else {
                  UIImageView *loadedImageView = [[UIImageView alloc] initWithImage:fetchedImage];
                  loadedImageView.frame = self.thumbnailImage.frame;
                  loadedImageView.alpha = 0;
                  [self.contentView addSubview:loadedImageView];
                                                                             
                  [UIView animateWithDuration:0.4
                        animations:^ {
                            loadedImageView.alpha = 1;
                            self.thumbnailImage.alpha = 0;
                        }
                        completion:^(BOOL finished) {
                            self.thumbnailImage.image = fetchedImage;
                            self.thumbnailImage.alpha = 1; 
                    }];
              }
                      
                      [self  bringSubviewToFront:self.viewDate];
        }
    }];
}

@end
