//
//  DiplomadosViewController.h
//  Story
//
//  Created by apple on 7/08/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DiplomadoDetalleViewController.h"
#import "CustomCellBackground.h"
#import "Common.h" 

@interface DiplomadosViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property(nonatomic,strong) IBOutlet UITableView *menuView;
-(IBAction)done:(id)sender;
@end
