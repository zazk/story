//
//  YoutubeViewController.h
//  Story
//
//  Created by zazk on 11/09/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VideoCell.h"
#import "NavegadorViewController.h"

@interface YoutubeViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property (strong, nonatomic) NSMutableArray *sets;
@property (strong, nonatomic) IBOutlet UITableView *menuView;
@property (strong, nonatomic) IBOutlet UILabel *lblTitulo;

@end
