//
//  ContactoViewController.h
//  Story
//
//  Created by apple on 23/08/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "Common.h"

@interface ContactoViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *menuView;
@property (strong, nonatomic) IBOutlet UILabel *lblTitulo;

@property (strong, nonatomic) IBOutlet UILabel *lblTitDireccion;
@property (strong, nonatomic) IBOutlet UIButton *btnDireccion;
@property (strong, nonatomic) IBOutlet UILabel *lblLlamenos;
@property (strong, nonatomic) IBOutlet UILabel *lblTitLlamenos;


@property (strong, nonatomic) IBOutlet UIImageView *imgLlamenos;
@property (strong, nonatomic) IBOutlet UIImageView *imgDireccion;
@end
