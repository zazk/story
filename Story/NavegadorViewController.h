//
//  NavegadorViewController.h
//  Story
//
//  Created by apple on 24/08/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"

@interface NavegadorViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIWebView *WebBrowser;
@property (strong, nonatomic) NSString *url;
@property (strong, nonatomic) NSString *html;

@end
