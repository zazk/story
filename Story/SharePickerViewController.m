//
//  SharePickerViewController.m
//  Story
//
//  Created by apple on 10/09/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import "SharePickerViewController.h"
 
@implementation SharePickerViewController

@synthesize colors,links,delegate;

- (void)viewDidLoad
{
    [super viewDidLoad];
 
    self.clearsSelectionOnViewWillAppear = NO;
    self.contentSizeForViewInPopover = CGSizeMake(150.0, 90.0);
    self.colors = [NSMutableArray array];
    [colors addObject:@"Facebook"];
    [colors addObject:@"Twitter"];
    self.links = [NSMutableArray array];
    [links addObject:@"https://www.facebook.com/sharer/sharer.php?u=%@&t=%@"];
    [links addObject:@"https://twitter.com/intent/tweet?url=%@&text=%@"];
}

- (void)viewDidUnload
{
    [super viewDidUnload]; 
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{  
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{  
    return [colors count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
     
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier ];
    }
     
    NSString *color = [colors objectAtIndex:indexPath.row];
    cell.textLabel.text = color;
    
    return cell;
}
 
#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{ 
    
    if (delegate != nil) {
        NSString *social = [links objectAtIndex:indexPath.row];
        [delegate  socialSelected:social];
    }
    
}
 
@end
