//
//  PostgradoEngine.m
//  Story
//
//  Created by zazk on 13/09/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import "PostgradoEngine.h"

@implementation PostgradoEngine

-(void) onCompletion:(PostgradoResponseBlock) appSyncBlock onError:(MKNKErrorBlock) errorBlock {
    NSLog(@"Postgrado URL SETS %@",POSTGRADO_URL);
    MKNetworkOperation *op = [self operationWithPath:POSTGRADO_URL];
    
    [op onCompletion:^(MKNetworkOperation *completedOperation) {
        
        NSMutableDictionary *response = [completedOperation responseJSON];
        
        appSyncBlock(response);
        
    } onError:^(NSError *error) {
        
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
}



-(NSString*) cacheDirectoryName {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *cacheDirectoryName = [documentsDirectory stringByAppendingPathComponent:@"PostgradoImages"];
    return cacheDirectoryName;
}

@end
