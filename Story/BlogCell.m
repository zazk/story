//
//  BlogCell.m
//  Story
//
//  Created by apple on 24/08/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import "BlogCell.h"

@implementation BlogCell
@synthesize imgBlogger;
@synthesize lblName;
@synthesize lblTitle;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
