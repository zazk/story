//
//  MediaCell.h
//  Story
//
//  Created by apple on 6/09/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MediaCell : UITableViewCell
@property (nonatomic, assign) IBOutlet UIImageView *thumbnailImage;
@property (nonatomic, assign) IBOutlet UILabel *titleLabel;
@property (nonatomic, assign) IBOutlet UILabel *authorNameLabel;

@property (nonatomic, strong) NSString* loadingImageURLString;
@property (nonatomic, strong) MKNetworkOperation* imageLoadingOperation;
-(void) setFlickrData:(NSDictionary*) thisFlickrImage;
@end
