//
//  EventoCell.h
//  Story
//
//  Created by zazk on 14/09/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "GMGridView.h"

@interface EventoCell : GMGridViewCell

@property (nonatomic, assign) IBOutlet UIImageView *thumbnailImage;
@property (strong, nonatomic) IBOutlet UILabel *lblDia;
@property (strong, nonatomic) IBOutlet UILabel *lblMes;
@property (strong, nonatomic) IBOutlet UILabel *lblTitulo;
@property (strong, nonatomic) IBOutlet UILabel *lblDetalle;
@property (strong, nonatomic) IBOutlet UIView *viewDate;

@property (nonatomic, strong) NSString* loadingImageURLString;
@property (nonatomic, strong) MKNetworkOperation* imageLoadingOperation;
-(void) setEventoData:(NSDictionary*) thisEventoImage;

@end
