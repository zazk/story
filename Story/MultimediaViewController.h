//
//  MultimediaViewController.h
//  Story
//
//  Created by apple on 5/09/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h> 
#import "GMGridView.h" 

@interface MultimediaViewController : UIViewController 
@property (strong, nonatomic) IBOutlet UIButton *imgGaleria;
@property (strong, nonatomic) IBOutlet UIButton *imgVideos;
@property (nonatomic, strong) IBOutlet UIButton *btnGaleria;
@property (nonatomic, strong) IBOutlet UIButton *btnVideos;

@end
