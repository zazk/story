//
//  PhotoCell.m
//  Story
//
//  Created by apple on 7/09/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import "PhotoCell.h"

@implementation PhotoCell
 
@synthesize thumbnailImage = thumbnailImage_;
@synthesize loadingImageURLString = loadingImageURLString_;
@synthesize imageLoadingOperation = imageLoadingOperation_;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

//===========================================================
// + (BOOL)automaticallyNotifiesObserversForKey:
//
//===========================================================
+ (BOOL)automaticallyNotifiesObserversForKey: (NSString *)theKey
{
    BOOL automatic;
    
    if ([theKey isEqualToString:@"thumbnailImage"]) {
        automatic = NO;
    } else {
        automatic = [super automaticallyNotifiesObserversForKey:theKey];
    }
    
    return automatic;
}
-(void) prepareForReuse {
    
    self.thumbnailImage.image = nil;
    [self.imageLoadingOperation cancel];
}


-(void) setFlickrData:(NSDictionary*) thisFlickrImage {
    
     
    self.loadingImageURLString =
    [NSString stringWithFormat:@"http://farm%@.static.flickr.com/%@/%@_%@_q.jpg",
     [thisFlickrImage objectForKey:@"farm"], [thisFlickrImage objectForKey:@"server"],
     [thisFlickrImage objectForKey:@"id"], [thisFlickrImage objectForKey:@"secret"]];
    NSLog(@"Load Complete URL: %@",self.loadingImageURLString);
    
    self.imageLoadingOperation = [ApplicationDelegate.flickrEngine imageAtURL:[NSURL URLWithString:self.loadingImageURLString]
        onCompletion:^(UIImage *fetchedImage, NSURL *url, BOOL isInCache) {
                  if([self.loadingImageURLString isEqualToString:[url absoluteString]]) {
              if (isInCache) {
                  self.thumbnailImage.image = fetchedImage;
              } else {
                  UIImageView *loadedImageView = [[UIImageView alloc] initWithImage:fetchedImage];
                  loadedImageView.frame = self.thumbnailImage.frame;
                  loadedImageView.alpha = 0;
                  [self.contentView addSubview:loadedImageView];
                                                                             
                  [UIView animateWithDuration:0.4
                        animations:^ {
                            loadedImageView.alpha = 1;
                            self.thumbnailImage.alpha = 0;
                        }
                        completion:^(BOOL finished) {
                            self.thumbnailImage.image = fetchedImage;
                            self.thumbnailImage.alpha = 1; 
                    }];
              }
        }
    }];
}

@end
