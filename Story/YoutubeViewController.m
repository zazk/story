//
//  YoutubeViewController.m
//  Story
//
//  Created by zazk on 11/09/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import "YoutubeViewController.h"

@interface YoutubeViewController ()

@end

@implementation YoutubeViewController

@synthesize sets;
@synthesize menuView;


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)loadView
{
    [super loadView];
        

}
- (void)viewDidLoad
{
    [super viewDidLoad];

	// Do any additional setup after loading the view.
        
    [ApplicationDelegate.youtubeEngine onCompletion:^(NSMutableArray* images) {
        
        self.sets = images; 
        [menuView reloadData];
    }
    onError:^(NSError* error) {
                                                
    }];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        
        self.lblTitulo.font = [UIFont boldSystemFontOfSize:18.0];
        [self.lblTitulo setFrame:CGRectMake(20,
                                            20,
                                            280,
                                            20)];
        [self.menuView setFrame:CGRectMake(20,
                                           40,
                                           280,
                                           400)];
        
    }
    self.view.backgroundColor =  [UIColor colorWithPatternImage:
                                  [UIImage imageNamed:@"bg-page.png"]];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}


- (void)viewWillAppear:(BOOL)animated
{
    [self setLandscapeImages];
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    /*
     imagesForKey:(NSString*) key subkey:(NSString*) subkey
     */
    
    [super viewDidAppear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}


-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    
    [self setLandscapeImages];
    
}

#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        return   80  ;
    }
    return   170  ;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
     
    return [ sets count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	VideoCell *cell = (VideoCell*)[tableView dequeueReusableCellWithIdentifier:@"YoutubeCell"];
    
    NSDictionary *thisYoutubeImage = [self.sets objectAtIndex:indexPath.row];
    
    
    // code from Stanford Tutorial
    [cell setYoutubeData:thisYoutubeImage];
    
	return cell;
}




#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    [self performSegueWithIdentifier:@"ShowVideo" sender:self];
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if ([segue.identifier isEqualToString:@"ShowVideo"]) {
        
        NSIndexPath *indexPath = [self.menuView indexPathForSelectedRow];
        NavegadorViewController  *destViewController = segue.destinationViewController;
        destViewController.title = [  [ [ [self.sets objectAtIndex:indexPath.row]  objectForKey:@"media$group"] objectForKey:@"media$title" ] objectForKey:@"$t" ] ;
        destViewController.html = [  [ [ [self.sets objectAtIndex:indexPath.row]  objectForKey:@"media$group"] objectForKey:@"yt$videoid" ] objectForKey:@"$t" ] ;
         
    }
}


-(void) setLandscapeImages{
    //bool isLandscape = UIDeviceOrientationIsLandscape([[UIDevice currentDevice] orientation]);
    
    self.view.backgroundColor =  [UIColor colorWithPatternImage:[UIImage imageNamed:isLandscape  ? @"bg-page-h.jpg": @"bg-page.png"]]; 
}

@end
