//
//  VideoCell.h
//  Story
//
//  Created by zazk on 11/09/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VideoCell : UITableViewCell
@property (nonatomic, assign) IBOutlet UIImageView *thumbnailImage;
@property (nonatomic, assign) IBOutlet UILabel *titleLabel;
@property (nonatomic, assign) IBOutlet UILabel *authorNameLabel;

@property (nonatomic, strong) NSString* loadingImageURLString;
@property (nonatomic, strong) NSString* idvideo;
@property (nonatomic, strong) MKNetworkOperation* imageLoadingOperation;
-(void) setYoutubeData:(NSDictionary*) thisYoutubeImage;
@end
