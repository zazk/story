//
//  AdmisionViewController.m
//  Story
//
//  Created by apple on 23/08/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import "AdmisionViewController.h"

@interface AdmisionViewController ()

@end

@implementation AdmisionViewController{
    NSMutableArray *menu;
    NSMutableArray *detalles;
    NSMutableArray *data;
}

@synthesize menuView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewDidAppear:(BOOL)animated{
    [self setLandscapeImages];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    data = [[[Store sharedInstance] myData] objectForKey:@"admision"];
    //data = nil;
    if ( data ) {
        NSLog(@"Data Loading in PostGrado View Controller:%@", data);
    }else{
        NSString *path = [[NSBundle mainBundle] pathForResource:@"Static" ofType:@"plist"];
        NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:path];
        menu = [dict objectForKey:@"Admision" ];
        detalles = [dict objectForKey:@"AdmisionDetalle" ];
    }
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        self.lblTitulo.font = [UIFont boldSystemFontOfSize:20.0];
    }
    [self setLandscapeImages];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (data) {
        return [data count];
    }
    return [ menu count];
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *menuID = @"ContactoItem";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:menuID];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:menuID ];
    }
    
    cell.backgroundView = [[CustomCellBackground alloc] init]  ;
    
    cell.textLabel.font = [UIFont boldSystemFontOfSize:23.0];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        cell.textLabel.font = [UIFont boldSystemFontOfSize:15.0];
    }
    cell.textLabel.backgroundColor = [ UIColor clearColor];
    cell.textLabel.numberOfLines = 2;
    cell.textLabel.textColor = [UIColor colorWithHexString:@"444444"];
    
    cell.imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"icon-admision-%d.png", indexPath.row] ];
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"indicator-x.png"]];
    cell.accessoryView = imageView;
    cell.imageView.contentMode = UITableViewCellStyleSubtitle;
    cell.contentView.superview.backgroundColor = [UIColor darkGrayColor];
    if (data) {
        cell.textLabel.text = [[data objectAtIndex:indexPath.row] objectForKey:@"title"];
    }else{
        cell.textLabel.text = [menu objectAtIndex:indexPath.row];
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        
        return   40  ;
        
    }
    return   77  ;
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"ShowAdmisionDetalle"]) {
        NSIndexPath *indexPath = [self.menuView indexPathForSelectedRow];
        PostgradoDetalleViewController  *destViewController = segue.destinationViewController;
        
        if (data) {
            destViewController.titulo = [[data objectAtIndex:indexPath.row] objectForKey:@"title"]; 
            
            destViewController.html = [NSString stringWithFormat:kBaseHTML, [[data objectAtIndex:indexPath.row] objectForKey:@"content"] ];
            
        }else{
            destViewController.titulo = [menu objectAtIndex:indexPath.row];
            destViewController.html = [detalles objectAtIndex:indexPath.row];
        }
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"ShowAdmisionDetalle" sender:self];
    
}

-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    
    [self setLandscapeImages];
    
}

-(void) setLandscapeImages{
    //bool isLandscape = UIDeviceOrientationIsLandscape([[UIDevice currentDevice] orientation]);
    
    self.view.backgroundColor =  [UIColor colorWithPatternImage:[UIImage imageNamed:isLandscape  ? @"bg-page-h.jpg": @"bg-page.png"]]; 
}


@end
