//
//  FlickrDetalleViewController.m
//  Story
//
//  Created by apple on 5/09/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import "FlickrDetalleViewController.h"

@interface FlickrDetalleViewController ()

@end

@implementation FlickrDetalleViewController
@synthesize thumbnailImage;
@synthesize loadingImageURLString;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.view.backgroundColor =  [UIColor blackColor];
    [self setFlickrData:self.loadingImageURLString];
}

- (void)viewDidUnload
{
    [self setThumbnailImage:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

-(void) setFlickrData:(NSString*) urlPhoto { 
    
    self.imageLoadingOperation = [ApplicationDelegate.flickrEngine imageAtURL:[NSURL URLWithString:self.loadingImageURLString]
        onCompletion:^(UIImage *fetchedImage, NSURL *url, BOOL isInCache) {
            if([self.loadingImageURLString isEqualToString:[url absoluteString]]) {
              if (isInCache) {
                  self.thumbnailImage.image = fetchedImage;
              } else {
                  UIImageView *loadedImageView = [[UIImageView alloc] initWithImage:fetchedImage];
                  loadedImageView.frame = self.thumbnailImage.frame;
                  loadedImageView.alpha = 0;
                  loadedImageView.contentMode = UIViewContentModeCenter;
                  [self.view addSubview:loadedImageView];
                  self.thumbnailImage = loadedImageView;                                                           
                  [UIView animateWithDuration:0.4
                        animations:^ {
                            loadedImageView.alpha = 1;
                            self.thumbnailImage.alpha = 0;
                        }
                        completion:^(BOOL finished) {
                            self.thumbnailImage.image = fetchedImage;
                            self.thumbnailImage.alpha = 1; 
                    }];
              }
        }
    }];
}
@end
