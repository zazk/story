//
//  BoletinViewController.m
//  Story
//
//  Created by apple on 31/08/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import "BoletinViewController.h"

@interface BoletinViewController ()

@end

@implementation BoletinViewController{
    NSMutableArray *menu; 
    NSMutableArray *urls;
}
@synthesize menuView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        self.lblTitulo.font= [UIFont boldSystemFontOfSize:18.0];
        [self.lblTitulo setFrame:CGRectMake(10,
                                            10,
                                            320,
                                            60)];
        
        [self.menuView setFrame:CGRectMake(0,
                                            100,
                                            320,
                                            320)];
        
    }
	// Do any additional setup after loading the view.
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Static" ofType:@"plist"];
    NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:path];
    menu = [dict objectForKey:@"Boletines" ]; 
    urls = [dict objectForKey:@"BoletinesURL" ];
    self.view.backgroundColor =  [UIColor colorWithPatternImage: [UIImage imageNamed:@"bg-page.png"]]; 
}

- (void)viewDidUnload
{
    [self setMenuView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

-(void)viewDidAppear:(BOOL)animated{
    [self setLandscapeImages];
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}
-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    
    [self setLandscapeImages];
    
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [ menu count];
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *menuID = @"SimpleTableCell";
    BoletinCell *cell = (BoletinCell *)[tableView dequeueReusableCellWithIdentifier:menuID];
    
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"BoletinCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    cell.textLabel.backgroundColor = [ UIColor clearColor];
    cell.backgroundView = [[CustomCellBackground alloc] init]  ;
    cell.lblTitle.text = [menu objectAtIndex:indexPath.row];
    
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        cell.lblTitle.font= [UIFont systemFontOfSize:11.0];
        [cell.imgArrow setFrame:CGRectMake(cell.imgArrow.bounds.origin.x,
                                            cell.imgArrow.bounds.origin.y,
                                            cell.imgArrow.bounds.size.width,		
                                            cell.imgArrow.bounds.size.height)];
        
    }
    cell.imgArrow.image = [UIImage imageNamed:[NSString stringWithFormat:@"indicator-boletin-0%d.png", indexPath.row] ];
    cell.imgIcon.image= [UIImage imageNamed:[NSString stringWithFormat:@"img-boletin-0%d.png", indexPath.row] ];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        
        return   60  ;
        
    }
    return   88  ; }

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"ShowBoletinDetalle"]) { 
        NSIndexPath *indexPath = [self.menuView indexPathForSelectedRow];
        NavegadorViewController  *destViewController = segue.destinationViewController;
        destViewController.title = [menu objectAtIndex:indexPath.row];
        destViewController.url = [urls objectAtIndex:indexPath.row];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{ 
    [self performSegueWithIdentifier:@"ShowBoletinDetalle" sender:self];
    
}

-(void) setLandscapeImages{
    //bool isLandscape = UIDeviceOrientationIsLandscape([[UIDevice currentDevice] orientation]);
    
    self.view.backgroundColor =  [UIColor colorWithPatternImage:[UIImage imageNamed:isLandscape  ? @"bg-page-h.jpg": @"bg-page.png"]];
}


@end
