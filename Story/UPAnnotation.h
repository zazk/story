//
//  UPAnnotation.h
//  Story
//
//  Created by zazk on 17/09/12.
//  Copyright (c) 2012 apple. All rights reserved.
//
 
#import <MapKit/MapKit.h>

@interface UPAnnotation : NSObject <MKAnnotation>

@end
