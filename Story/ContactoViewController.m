//
//  ContactoViewController.m
//  Story
//
//  Created by apple on 23/08/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import "ContactoViewController.h"
#import "DarkCellBackgroundView.h"
#import "UIColor-Expanded.h"
#import "PostgradoDetalleViewController.h"

@interface ContactoViewController ()

@end

@implementation ContactoViewController{
    NSMutableArray *menu;
    NSMutableArray *detalles;
    NSMutableArray *data;
}

@synthesize menuView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidAppear:(BOOL)animated{
    
    [self setLandscapeImages];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    data = [[[Store sharedInstance] myData] objectForKey:@"contacto"];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        self.lblTitulo.font= [UIFont boldSystemFontOfSize:18.0];
        
        
        self.lblLlamenos.font=  self.lblTitLlamenos.font=  self.lblTitDireccion.font=  self.btnDireccion.titleLabel.font= [UIFont boldSystemFontOfSize:12.0];
        
        [self.lblTitulo setFrame:CGRectMake(20,
                                            10,
                                            320,
                                            30)];
        
        
        [self.menuView setFrame:CGRectMake(20,
                                            180,
                                            280,
                                            100)];
        
        
        
        [self.lblLlamenos setFrame:CGRectMake(40,
                                            70,
                                            320,
                                            20)];
        [self.lblTitLlamenos setFrame:CGRectMake(20,
                                            50,
                                            320,
                                            20)];
        [self.imgLlamenos setFrame:CGRectMake(20,
                                            70,
                                            15,
                                            15)];
        
        [self.btnDireccion setFrame:CGRectMake(10,
                                            120,
                                            320,
                                            20)];
        [self.lblTitDireccion setFrame:CGRectMake(20,
                                            100,
                                            320,
                                            20)];
        [self.imgDireccion setFrame:CGRectMake(20,
                                            120,
                                            15,
                                            15)];
        
    }
    //data = nil;
    if ( data ) {
        NSLog(@"Data Loading in Contacto View Controller:%@", data);
    }else{ 
        NSString *path = [[NSBundle mainBundle] pathForResource:@"Static" ofType:@"plist"];
        NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:path];
        menu = [dict objectForKey:@"Contacto" ];
        detalles = [dict objectForKey:@"ContactoDetalle" ];
    } 
    
    [self setLandscapeImages];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (data) {
        return [data count];
    }
    return [ menu count];
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *menuID = @"PosgradoItem";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:menuID];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:menuID ];
    }
    cell.backgroundView = [[DarkCellBackgroundView alloc] init]  ;
     
    cell.textLabel.font = [UIFont boldSystemFontOfSize:23.0];
    cell.textLabel.backgroundColor = [ UIColor clearColor]; 
    cell.textLabel.textColor = [UIColor colorWithHexString:@"ffffff"];
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"indicator-x.png"]];
    cell.accessoryView = imageView;
    cell.imageView.contentMode = UITableViewCellStyleSubtitle;
    cell.contentView.superview.backgroundColor = [UIColor darkGrayColor];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        cell.textLabel.font= [UIFont boldSystemFontOfSize:14.0];
    }
    if (data) {
        cell.textLabel.text = [[data objectAtIndex:indexPath.row] objectForKey:@"title"];
    }else{
        cell.textLabel.text = [menu objectAtIndex:indexPath.row];
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        return 35;
    }
    return   55  ; }


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"ShowContactoDetalle"]) {
        NSIndexPath *indexPath = [self.menuView indexPathForSelectedRow];
        PostgradoDetalleViewController  *destViewController = segue.destinationViewController; 
         
        if (data) {
            destViewController.titulo = [[data objectAtIndex:indexPath.row] objectForKey:@"title"]; 
            
            destViewController.html = [NSString stringWithFormat:kBaseHTML, [[data objectAtIndex:indexPath.row] objectForKey:@"content"] ];
            
        }else{
            destViewController.titulo = [menu objectAtIndex:indexPath.row];
            destViewController.html = [detalles objectAtIndex:indexPath.row];
        }
    }
}
 
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"ShowContactoDetalle" sender:self];
    
}

-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    
    [self setLandscapeImages];
    
}
-(void) setLandscapeImages{
    //bool isLandscape = UIDeviceOrientationIsLandscape([[UIDevice currentDevice] orientation]);
    self.view.backgroundColor =  [UIColor colorWithPatternImage:[UIImage imageNamed:isLandscape  ? @"bg-contacto-h.jpg": @"bg-contacto.jpg"]];
    
}

@end
