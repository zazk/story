//
//  MaestriasViewController.h
//  Story
//
//  Created by apple on 8/08/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIColor-Expanded.h"

@interface MaestriasViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>{

}

@property(nonatomic,strong) IBOutlet UITableView *menuView;
-(IBAction)done:(id)sender; 


@end
