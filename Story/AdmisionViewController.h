//
//  AdmisionViewController.h
//  Story
//
//  Created by apple on 23/08/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import <UIKit/UIKit.h> 
#import "PostgradoDetalleViewController.h"
#import "CustomCellBackground.h"
#import "UIColor-Expanded.h"
#import "Constants.h"

@interface AdmisionViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *menuView;
@property (strong, nonatomic) IBOutlet UILabel *lblTitulo;
@end
