	//
//  main.m
//  Story
//
//  Created by apple on 5/08/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "StoryAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([StoryAppDelegate class]));
    }
}
	