//
//  NoticiasDetalleViewController.h
//  Story
//
//  Created by apple on 3/09/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import <UIKit/UIKit.h> 
#import "Common.h"
#import "NavegadorViewController.h"
#import "SharePickerViewController.h"

@interface NoticiasDetalleViewController : UIViewController
    <SharePickerDelegate,UITableViewDataSource,UITableViewDelegate,UIWebViewDelegate>

@property (strong, nonatomic) IBOutlet UILabel *lblTitulo;
@property (strong, nonatomic) IBOutlet UILabel *lblFecha;
@property (strong, nonatomic) IBOutlet UIImageView *imgNoticia; 
@property (strong, nonatomic) IBOutlet UIWebView *webDetalle;

@property (strong, nonatomic) NSString *link,*url,*detalle,*titulo,*galeria,*imagen,*fecha,*enlace;
@property (nonatomic,strong) MKNetworkOperation* imageLoadingOperation;

@property (nonatomic, retain) SharePickerViewController *sharePicker;
@property (nonatomic, retain) UIPopoverController *sharePickerPopover;

-(void) setFlickrData:(NSString*) urlPhoto;
@end
