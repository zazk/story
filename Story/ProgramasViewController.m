//
//  ProgramasViewController.m
//  Story
//
//  Created by apple on 7/08/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import "ProgramasViewController.h"

@interface ProgramasViewController ()

@end

@implementation ProgramasViewController{
    NSMutableArray *menu;
}

@synthesize menuView,imageDiplomado,imageMaestria;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void) viewDidAppear:(BOOL)animated{
    
    [self setLandscapeImages];
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.navigationController.view.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:1.0];
    
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(receivedRotate:) name: UIDeviceOrientationDidChangeNotification object: nil];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        self.lblTitulo.font= [UIFont boldSystemFontOfSize:14.0];
        [self.lblTitulo setFrame:CGRectMake(10,
                                            10,
                                            320,
                                            60)];
        self.btnDiplomado.titleLabel.font = [UIFont boldSystemFontOfSize:16.0];
        self.btnMaestria.titleLabel.font = [UIFont boldSystemFontOfSize:16.0];
        self.lblTitulo.numberOfLines = 3;
        
    }
    
    [self setLandscapeImages];
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}


-(void) receivedRotate: (NSNotification*) notification
{
    [self setLandscapeImages];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

-(IBAction)done:(id)sender{
    [self dismissModalViewControllerAnimated:YES];
}

-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    
    [self setLandscapeImages];
    
}

-(void) setLandscapeImages{
    //bool isLandscape = UIDeviceOrientationIsLandscape([[UIDevice currentDevice] orientation]);
    
    NSLog(@"%@",@">>>>>>>>>>");
    NSLog(isLandscape ? @"IsLandScape: Yes" : @"Is LandScape:		No"); 
    NSLog(@"%@",@">>>>>>>>>>");
    self.view.backgroundColor =  [UIColor colorWithPatternImage:[UIImage imageNamed:isLandscape  ? @"bg-page-h.jpg": @"bg-page.png"]];
    
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) { 
        imageMaestria.backgroundColor = [UIColor colorWithPatternImage:[self setBackground:@"img-maestrias.jpg"]];
        imageDiplomado.backgroundColor =[UIColor colorWithPatternImage:[self setBackground:@"img-diplomados.jpg"]];

        
    }else{
        
        imageMaestria.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:isLandscape  ? @"img-maestria-h.jpg": @"img-maestrias.jpg"]];
        imageDiplomado.backgroundColor =[UIColor colorWithPatternImage:[UIImage imageNamed:isLandscape ? @"img-diplomado-h.jpg": @"img-diplomados.jpg"]];
    }
}


-(UIImage *)  setBackground: (NSString*) image{
     
        UIGraphicsBeginImageContext(imageMaestria.bounds.size);
        [[UIImage imageNamed:image] drawInRect:imageMaestria.bounds];
        UIImage *bg = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    return bg;
         
}


-(BOOL) webView:(UIWebView *)inWeb shouldStartLoadWithRequest:(NSURLRequest *)inRequest navigationType:(UIWebViewNavigationType)inType {
    if ( inType == UIWebViewNavigationTypeLinkClicked ) {
        [[UIApplication sharedApplication] openURL:[inRequest URL]];
        return NO;
    }
    
    return YES;
}

@end
