//
//  NoticiaCell.h
//  Story
//
//  Created by apple on 3/09/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NoticiaCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imgNoticia;
@property (strong, nonatomic) IBOutlet UILabel *lblTitulo;
@property (strong, nonatomic) IBOutlet UILabel *lblDetalle;

@property (nonatomic, strong) NSString* loadingImageURLString;
@property (nonatomic, strong) MKNetworkOperation* imageLoadingOperation;
-(void) setNoticiaData:(NSDictionary*) thisNoticiaImage;

@end
