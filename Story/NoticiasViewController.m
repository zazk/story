//
//  NoticiasViewController.m
//  Story
//
//  Created by apple on 3/09/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import "NoticiasViewController.h"

@interface NoticiasViewController ()

@end

@implementation NoticiasViewController

@synthesize menuView,sets;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewDidAppear:(BOOL)animated{
    [self setLandscapeImages];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    int pagina = 0;
    int mostrar = 10;
    
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        
        self.lblTitulo.font = [UIFont boldSystemFontOfSize:18.0];
        [self.lblTitulo setFrame:CGRectMake(30,
                                           20,
                                           280,
                                           20)];
        [self.menuView setFrame:CGRectMake(20,
                                             40,
                                             280,
                                             400)];
        
    }
     	
    [ApplicationDelegate.noticiasEngine
     newsWithIndex:pagina
     andPage:mostrar
     onCompletion:^(NSMutableArray* noticias) {
         self.sets = noticias;
         [menuView reloadData];
         
    }
    onError:^(NSError* error) {
        
    }];
    
    [self setLandscapeImages];
}

- (void)viewDidUnload
{
    [self setMenuView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [ sets count];
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *menuID = @"SimpleCell";
    NoticiaCell *cell = (NoticiaCell *)[tableView dequeueReusableCellWithIdentifier:menuID];
    
    
    /*
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"NoticiaCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    cell.lblTitulo.text = [menu objectAtIndex:indexPath.row];
    cell.imgNoticia.image = [UIImage imageNamed:[NSString stringWithFormat:@"img-noticias-0%d.jpg", indexPath.row] ];
    cell.lblDetalle.text = [detalles objectAtIndex:indexPath.row];
    
    */
    NSDictionary *thisNoticiaImage = [self.sets objectAtIndex:indexPath.row];
    
    // code from Stanford Tutorial
    [cell setNoticiaData:thisNoticiaImage];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        return   80  ;
    }
    return   170  ; }

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{ 
    [self performSegueWithIdentifier:@"ShowNoticia" sender:self];
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"ShowNoticia"]) {
        
        NSIndexPath *indexPath = [self.menuView indexPathForSelectedRow];
        NoticiasDetalleViewController  *destViewController = segue.destinationViewController;
        destViewController.url = [ [sets objectAtIndex:indexPath.row] objectForKey:@"urlApp"];
        destViewController.detalle = [ [sets objectAtIndex:indexPath.row] objectForKey:@"contenido"];
        destViewController.titulo = [ [sets objectAtIndex:indexPath.row] objectForKey:@"titulo"];
        destViewController.galeria = [ [sets objectAtIndex:indexPath.row] objectForKey:@"urlApp"]; 
        destViewController.imagen = [ [sets objectAtIndex:indexPath.row] objectForKey:@"imagenApp"];
        destViewController.fecha = [ [sets objectAtIndex:indexPath.row] objectForKey:@"fechaApp"];
        
    }
}

-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    
    [self setLandscapeImages];
    
}

-(void) setLandscapeImages{
    //bool isLandscape = UIDeviceOrientationIsLandscape([[UIDevice currentDevice] orientation]);
    
    self.view.backgroundColor =  [UIColor colorWithPatternImage:[UIImage imageNamed:isLandscape  ? @"bg-page-h.jpg": @"bg-page.png"]];
}

@end
