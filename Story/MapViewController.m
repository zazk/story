//
//  MapViewController.m
//  Story
//
//  Created by zazk on 17/09/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import "MapViewController.h"

@interface MapViewController ()

@end

@implementation MapViewController
@synthesize mapViewer,upAnnotation;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    

    
}
- (void)viewWillAppear:(BOOL)animated {
    [self gotoLocation];
}

- (void)viewDidUnload
{
    [self setMapViewer:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
    
        MKPinAnnotationView *annotationView = (MKPinAnnotationView *) [mapViewer dequeueReusableAnnotationViewWithIdentifier:@"MapViewer"];
        if (annotationView == nil) {
            annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"MapViewer"];
        } else {
            annotationView.annotation = annotation;
        }
        
        annotationView.enabled = YES;
        annotationView.canShowCallout = YES;
        
        return annotationView;
        
}

- (void)gotoLocation
{ 
    MKCoordinateRegion newRegion;
    newRegion.center.latitude = -12.083889;
    newRegion.center.longitude = -77.048333;
    newRegion.span.latitudeDelta = 0.01;
    newRegion.span.longitudeDelta = 0.01;
    
     
    UPAnnotation *pin = [[UPAnnotation alloc] init];
    [self.mapViewer addAnnotation:pin];
    [self.mapViewer setRegion:newRegion animated:YES];
}

@end
