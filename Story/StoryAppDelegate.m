//
//  StoryAppDelegate.m
//  Story
//
//  Created by apple on 5/08/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import "StoryAppDelegate.h"

@implementation StoryAppDelegate

@synthesize flickrEngine;
@synthesize youtubeEngine;
@synthesize postgradoEngine;
@synthesize eventosEngine;
@synthesize noticiasEngine;


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    self.flickrEngine = [[FlickrEngine alloc] initWithHostName:@"api.flickr.com" customHeaderFields:nil];
    [self.flickrEngine useCache];
    self.youtubeEngine = [[YoutubeEngine alloc] initWithHostName:@"gdata.youtube.com" customHeaderFields:nil];
    [self.youtubeEngine useCache];
    self.postgradoEngine = [[PostgradoEngine alloc] initWithHostName:@"www.up.edu.pe" customHeaderFields:nil];
    [self.postgradoEngine useCache];
    self.eventosEngine = [[EventosEngine alloc] initWithHostName:@"www.up.edu.pe" customHeaderFields:nil];
    [self.eventosEngine useCache];
    self.noticiasEngine = [[NoticiasEngine alloc] initWithHostName:@"www.up.edu.pe" customHeaderFields:nil];
    [self.noticiasEngine useCache];
    
 
    /*
                                                 */
    sleep(0);
    
    if( [[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0f ) {
        [[UINavigationBar appearance] setBarTintColor:[UIColor grayColor]];
    }
    return YES;
}
 
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
