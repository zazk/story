//
//  PhotoCell.h
//  Story
//
//  Created by apple on 7/09/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import "GMGridView.h"

@interface PhotoCell : GMGridViewCell

@property (nonatomic, assign) IBOutlet UIImageView *thumbnailImage; 

@property (nonatomic, strong) NSString* loadingImageURLString;
@property (nonatomic, strong) MKNetworkOperation* imageLoadingOperation;
-(void) setFlickrData:(NSDictionary*) thisFlickrImage;

@end
