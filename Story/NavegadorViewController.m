//
//  NavegadorViewController.m
//  Story
//
//  Created by apple on 24/08/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import "NavegadorViewController.h"

@interface NavegadorViewController ()

@end

@implementation NavegadorViewController
@synthesize WebBrowser;
@synthesize url;
@synthesize html;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    //Create a URL object.
    
    if (url) {
        
        NSURL *urlObj = [NSURL URLWithString:url];
    
        //URL Requst Object
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:urlObj];
    
        //Load the request in the UIWebView.
        [WebBrowser loadRequest:requestObj];
    
        
    }else{
        //Load HTML
        
        NSString *videohtml = kEmbedHTML;
        if   (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            videohtml = kEmbedHTMLiPhone;
        }
        
        NSString *result =  [NSString stringWithFormat: videohtml , html, WebBrowser.frame.size.width+210,WebBrowser.frame.size.height+210 ] ;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            result =  [NSString stringWithFormat: videohtml , html, WebBrowser.frame.size.width +610,WebBrowser.frame.size.height +610 ] ;
        }
        [WebBrowser loadHTMLString:result baseURL:nil];
        WebBrowser.backgroundColor = [UIColor blackColor];
    }
    //if (![self.title isEqual: @"Navegador"]) {
    //    self.title= @" ";
    //}
    self.view.backgroundColor =  [UIColor blackColor];
}

- (void)viewDidUnload
{
    [self setWebBrowser:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    
    [self setLandscapeImages];
    
}

-(void) setLandscapeImages{ 
    if (url) {
         
    }else{
        //Load HTML
        NSString *result =  [NSString stringWithFormat: kEmbedHTML , html, WebBrowser.frame.size.width+210,WebBrowser.frame.size.height+210 ] ;
        [WebBrowser loadHTMLString:result baseURL:nil];
    }
 
}
@end
