//
//  UPAnnotation.m
//  Story
//
//  Created by zazk on 17/09/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import "UPAnnotation.h"

@implementation UPAnnotation

- (CLLocationCoordinate2D)coordinate;
{
    CLLocationCoordinate2D theCoordinate;
    theCoordinate.latitude = -12.083889;
    theCoordinate.longitude = -77.048333;
    return theCoordinate;
}

// required if you set the MKPinAnnotationView's "canShowCallout" property to YES
- (NSString *)title
{
    return @"Universidad del Pacífico";
}
 
// optional
- (NSString *)subtitle
{
    return @"Escuela de Postgrado";
}
 


@end
