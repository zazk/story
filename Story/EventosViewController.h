//
//  EventosViewController.h
//  Story
//
//  Created by apple on 31/08/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "GMGridView/GMGridView.h"
#import "EventoCell.h"
#import "EventoDetalleViewController.h"

@interface EventosViewController : UIViewController<GMGridViewDataSource, GMGridViewSortingDelegate, GMGridViewTransformationDelegate,
GMGridViewActionDelegate>
 

@property (nonatomic, assign) int count,lastTappedIndex;
@property (strong, nonatomic) NSMutableArray *photos;
@property (strong, nonatomic) NSString *year,*month;

- (void)addMoreItem;
- (void)removeItem;
- (void)refreshItem;
- (void)presentOptions:(UIBarButtonItem *)barButton;
- (void)optionsDoneAction;
- (void)dataSetChange:(UISegmentedControl *)control;
@end
