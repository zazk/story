//
//  DiplomadosViewController.m
//  Story
//
//  Created by apple on 7/08/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import "DiplomadosViewController.h" 

@interface DiplomadosViewController ()

@end

@implementation DiplomadosViewController{
    NSMutableArray *menu,*detalles,*urls,*brochures;
    NSMutableArray *data;
}

@synthesize menuView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    data = [[[Store sharedInstance] myData] objectForKey:@"diplomados"];
    //data = nil;
    if ( data ) {
        NSLog(@"Data Loading in Diplomados View Controller:%@", data);
    }else{
        NSString *path = [[NSBundle mainBundle] pathForResource:@"Static" ofType:@"plist"]; 
        NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:path];
        menu = [dict objectForKey:@"Diplomados" ];
        detalles = [dict objectForKey:@"DiplomadosDetalle" ];
        urls = [dict objectForKey:@"DiplomadosURL" ];
        brochures = [dict objectForKey:@"DiplomadosBrochure" ];
    }
    menuView.backgroundColor =  [UIColor colorWithPatternImage: [UIImage imageNamed:@"bg-page.png"]];
    
    
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (data) {
        return [data count];
    }
    return [ menu count];
}


-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *menuID = @"DiplomadosItem";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:menuID];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:menuID ];
    }
    cell.backgroundView = [[CustomCellBackground alloc] init]  ;
     
    cell.textLabel.backgroundColor = [ UIColor clearColor];
    cell.textLabel.numberOfLines = 2;
    cell.textLabel.textColor = [UIColor colorWithRed:70/255.0 green:70/255.0 blue:70/255.0 alpha:1];  
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        cell.textLabel.font = [UIFont boldSystemFontOfSize:15.0];
    }
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"indicator-x.png"]];
    cell.imageView.image =  [UIImage imageNamed:@"img-x.png" ];
    cell.accessoryView = imageView;
    cell.imageView.contentMode = UITableViewCellStyleSubtitle;
    cell.contentView.superview.backgroundColor = [UIColor darkGrayColor];
     
    
    if (data) {
        cell.textLabel.text = [[data objectAtIndex:indexPath.row] objectForKey:@"title"];
    }else{
        cell.textLabel.text = [menu objectAtIndex:indexPath.row];
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        return   50  ;
    }
    return   80  ;
}
 

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"ShowDiplomadoDetalle"]) {
        
        NSIndexPath *indexPath = [self.menuView indexPathForSelectedRow];
        DiplomadoDetalleViewController  *destViewController = segue.destinationViewController;
        
        if (data) {
            NSDictionary *row = [data objectAtIndex:indexPath.row];
            destViewController.maestriaName = [row objectForKey:@"title"];
            destViewController.detalle = [row objectForKey:@"content"];  
            destViewController.url =[row objectForKey:@"url"];
            destViewController.brochure =[row objectForKey:@"brochure"];
            destViewController.loadingImageURLString =[row objectForKey:@"imagenApp"];
        }else{
            destViewController.maestriaName = [menu objectAtIndex:indexPath.row];
            destViewController.detalle = [detalles objectAtIndex:indexPath.row];
            destViewController.url = [urls objectAtIndex:indexPath.row];
            destViewController.brochure = [brochures objectAtIndex:indexPath.row];
            destViewController.imagen = [NSString stringWithFormat:@"diplomado-%d.jpg", indexPath.row];
        }
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

-(IBAction)done:(id)sender{
    [self dismissModalViewControllerAnimated:YES];
}

@end
