//
//  BoletinCell.h
//  Story
//
//  Created by apple on 1/09/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BoletinCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imgIcon;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle; 
@property (strong, nonatomic) IBOutlet UIImageView *imgArrow;

@end
