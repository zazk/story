//
//  PostgradoViewController.m
//  Story
//
//  Created by apple on 23/08/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import "PostgradoViewController.h"
#import "PostgradoDetalleViewController.h"
#import "UIColor-Expanded.h"
#import "CustomCellBackground.h"

@interface PostgradoViewController ()

@end

@implementation PostgradoViewController{
    NSMutableArray *menu;
    NSMutableArray *detalles;
    NSMutableArray *data;
}
@synthesize menuView;
@synthesize webVideo;
@synthesize items;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self; 
}

- (void) viewDidAppear:(BOOL)animated{
    
    [self setLandscapeImages];
    	
}

- (void)viewDidLoad
{
    [self setLandscapeImages];
    [super viewDidLoad]; 
    data = [[[Store sharedInstance] myData] objectForKey:@"postgrado"];
    //data = nil;
    if ( data ) {
        NSLog(@"Data Loading in PostGrado View Controller:%@", data);
    }else{
        
        NSString *path = [[NSBundle mainBundle] pathForResource:@"Static" ofType:@"plist"];
        NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:path];
        
        menu = [dict objectForKey:@"Postgrado" ];
        detalles = [dict objectForKey:@"PostgradoDetalle" ];
    }
    
    
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) { 
        
        self.lblTitulo.font = [UIFont boldSystemFontOfSize:15.0];
        [self.lblTitulo setFrame:CGRectMake(10,
                                            10,
                                            300,
                                            40)];
        [self.webVideo setFrame:CGRectMake(10,
                                            60,
                                            self.lblTitulo.bounds.size.width, 
                                           180)];
        [self.menuView setFrame:CGRectMake(10,
                                           260,
                                           self.lblTitulo.bounds.size.width,
                                           180)]; 

    }
    
    [self embedYouTube:@"Qg4Kwyvpo_I"];
    /*
    self.lblTitulo.font = [UIFont boldSystemFontOfSize:13.0];
    [self.lblTitulo setFrame:CGRectMake(0,
                                       0,
                                       150,
                                       menuView.bounds.size.height)];*/
}

- (void)viewDidUnload
{
    [self setMenuView:nil];
    [self setWebVideo:nil];
    [self setLblTitulo:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}


- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (data) {
        return [data count];
    }
    return [ menu count];
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *menuID = @"PosgradoItem";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:menuID];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:menuID ];
    }
    cell.backgroundView = [[CustomCellBackground alloc] init]  ;
    
    cell.textLabel.font = [UIFont boldSystemFontOfSize:23.0];
    
    if   (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        
        cell.textLabel.font = [UIFont boldSystemFontOfSize:15.0]; 
        
    }
    cell.textLabel.backgroundColor = [ UIColor clearColor];
    cell.textLabel.numberOfLines = 2;
    cell.textLabel.textColor = [UIColor colorWithHexString:@"444444"];
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"indicator-x.png"]]; 
    cell.accessoryView = imageView;
    cell.imageView.contentMode = UITableViewCellStyleSubtitle;
    cell.contentView.superview.backgroundColor = [UIColor darkGrayColor];
    
    if (data) {
        cell.textLabel.text = [[data objectAtIndex:indexPath.row] objectForKey:@"title"];
    }else{ 
        cell.textLabel.text = [menu objectAtIndex:indexPath.row];
    } 
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    
    if   (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        return 40;
    }
    
    return   50  ;

}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"ShowPostgradoDetalle"]) {  
        NSIndexPath *indexPath = [self.menuView indexPathForSelectedRow];
        PostgradoDetalleViewController  *destViewController = segue.destinationViewController;
        if (data) {
            destViewController.titulo = [[data objectAtIndex:indexPath.row] objectForKey:@"title"]; 
            
            NSString *html = kHtmlWithYoutube;
            if   (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                html = kHtmlWithYoutubeiPhone;
            }
            
            destViewController.html = [NSString stringWithFormat:html, [[data objectAtIndex:indexPath.row] objectForKey:@"content"], getYoutubeID([[data objectAtIndex:indexPath.row] objectForKey:@"mediaApp"] )];
             
        }else{
            destViewController.titulo = [menu objectAtIndex:indexPath.row];
            destViewController.html = [detalles objectAtIndex:indexPath.row];
        }
    }
}
  
-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    
    [self setLandscapeImages];
    
}

- (void)embedYouTube:(NSString *)urlString   { 
    NSString *html = [NSString stringWithFormat:kEmbedYoutubeHTML , urlString, webVideo.frame.size.width, webVideo.frame.size.height];
    [webVideo loadHTMLString:html baseURL:nil];
}

-(void) setLandscapeImages{
    //bool isLandscape = UIDeviceOrientationIsLandscape([[UIDevice currentDevice] orientation]);
    self.view.backgroundColor =  [UIColor colorWithPatternImage:[UIImage imageNamed:isLandscape  ? @"bg-page-h.jpg": @"bg-page.png"]];
}

@end
