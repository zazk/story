//
//  PostgradoEngine.h
//  Story
//
//  Created by zazk on 13/09/12.
//  Copyright (c) 2012 apple. All rights reserved.
//
 
#import "MKNetworkEngine.h" 
#define POSTGRADO_URL @"postgrado/_vti_bin/UP.GIIT.Portal.EPG/EPGAppService.svc/postgrado/" 

@interface PostgradoEngine : MKNetworkEngine

typedef void (^PostgradoResponseBlock)(NSMutableDictionary* imageURLs);
-(void) onCompletion:(PostgradoResponseBlock) appSyncBlock
             onError:(MKNKErrorBlock) errorBlock;
@end
