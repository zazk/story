//
//  BlogCell.h
//  Story
//
//  Created by apple on 24/08/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BlogCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imgBlogger;
@property (strong, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UIView *bgCell;

@end
