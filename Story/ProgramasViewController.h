//
//  ProgramasViewController.h
//  Story
//
//  Created by apple on 7/08/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProgramasViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIWebViewDelegate>
@property(nonatomic,strong) IBOutlet UITableView *menuView;
@property (nonatomic, strong) IBOutlet UIButton *imageMaestria;
@property (nonatomic, strong) IBOutlet UIButton *imageDiplomado;
@property (nonatomic, strong) IBOutlet UIButton *btnMaestria;
@property (nonatomic, strong) IBOutlet UIButton *btnDiplomado;
@property (strong, nonatomic) IBOutlet UILabel *lblTitulo;
-(IBAction)done:(id)sender;

@end
