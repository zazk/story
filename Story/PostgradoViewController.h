//
//  PostgradoViewController.h
//  Story
//
//  Created by apple on 23/08/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "Common.h"

@interface PostgradoViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *menuView;
@property (strong, nonatomic) IBOutlet UIWebView *webVideo;
@property (strong, nonatomic) IBOutlet UILabel *lblTitulo;
@property (strong, nonatomic) NSMutableArray *items;
- (void)embedYouTube:(NSString *)urlString;
@end
