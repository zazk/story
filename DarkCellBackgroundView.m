//
//  DarkCellBackgroundView.m
//  Story
//
//  Created by apple on 23/08/12.
//  Copyright (c) 2012 apple. All rights reserved.
//

#import "DarkCellBackgroundView.h"
#import "UIColor-Expanded.h"
#import "Common.h"

@implementation DarkCellBackgroundView
 
/*
// Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 */
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    drawLinearGradient(UIGraphicsGetCurrentContext(),
                       self.bounds,
                       [UIColor colorWithHexString:@"666666"].CGColor,
                       [UIColor colorWithHexString:@"000000"].CGColor);
}

@end
