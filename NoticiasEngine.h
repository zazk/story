//
//  NoticiasEngine.h
//  Story
//
//  Created by zazk on 15/09/12.
//  Copyright (c) 2012 apple. All rights reserved.
//
 

#import "MKNetworkEngine.h"
#define NOTICIAS_URL(__INDEX__, __PAGE__) [NSString stringWithFormat:@"postgrado/_vti_bin/UP.GIIT.Portal.EPG/EPGAppService.svc/noticia/?pageIndex=%d&itemsPerPage=%d", __INDEX__, __PAGE__]

@interface NoticiasEngine : MKNetworkEngine

typedef void (^NoticiaResponseBlock)(NSMutableArray* imageURLs);
-(void)newsWithIndex:(int) index
           andPage:(int) page
       onCompletion:(NoticiaResponseBlock) imageURLBlock
            onError:(MKNKErrorBlock) errorBlock;
@end
