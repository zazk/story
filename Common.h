//
//  Common.h
//  Cool
//
//  Created by apple on 28/07/12.
//
//

#import <Foundation/Foundation.h> 


#define kHighlightTop 0
#define kHighlightBottom 1

CGRect rectFor1PxStroke(CGRect rect);
void logRect(NSString *prefix, CGRect rect);

void draw1PxStroke(CGContextRef context, CGPoint startPoint, CGPoint endPoint,
                   CGColorRef color);
void drawLinearGradient(CGContextRef context, CGRect rect, CGColorRef startColor, CGColorRef endColor);

void drawGlossAndGradient(CGContextRef context, CGRect rect, CGColorRef startColor,
                          CGColorRef endColor);



static inline double radians (double degrees) { return degrees * M_PI/180; }
CGMutablePathRef createArcPathFromBottomOfRect(CGRect rect, CGFloat arcHeight);
CGMutablePathRef createRoundedRectForRect(CGRect rect, CGFloat radius);

  
void drawHighlightAndGradient(CGContextRef context, CGRect rect, CGColorRef startColor, CGColorRef endColor, BOOL reverse);
void drawLinearGloss(CGContextRef context, CGRect rect, BOOL reverse);
void drawCurvedGloss(CGContextRef context, CGRect rect, CGFloat radius); 
CGMutablePathRef createArcPathFromBottomOfRect(CGRect rect, CGFloat arcHeight);
CGMutablePathRef createRoundedRectForRect(CGRect rect, CGFloat radius);
CGMutablePathRef createRoundedRectForRectCCW(CGRect rect, CGFloat radius);
NSNumberFormatter *standardNumberFormatter();

NSString *getYoutubeID(NSString *str);
NSString *replace(NSString *str, int index);

//Add Buttons
UIButton *addButton(id target, NSString *title, CGRect rect, SEL selector);
void addButtonWithImage(id target, NSString *img, CGRect rect, SEL selector);
//Encoding
NSString *encodeURL( NSString*string );
